package vn.edu.hust.dsd.dsd.views.meetings

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.MeetingsMeetingCardBinding
import vn.edu.hust.dsd.dsd.models.meeting.entities.MeetingModel
import vn.edu.hust.dsd.dsd.viewmodels.MeetingsMeetingViewModel
import java.lang.ref.WeakReference

class MeetingsAdapter(fragment: Fragment): ListAdapter<MeetingModel, MeetingsAdapter.ViewHolder>(object: DiffUtil.ItemCallback<MeetingModel>(){
    override fun areItemsTheSame(oldItem: MeetingModel, newItem: MeetingModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: MeetingModel, newItem: MeetingModel): Boolean {
        return oldItem == newItem
    }
}){
    private val fragmentRef: WeakReference<Fragment> = WeakReference(fragment)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.meetings_meeting_card, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let{meeting ->
            fragmentRef.get()?.let { holder.bind(meeting, it) }
        }
    }

    class ViewHolder(private val binding: MeetingsMeetingCardBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(
            meeting: MeetingModel,
            fragment: Fragment
        ) {
            binding.viewModel = MeetingsMeetingViewModel(meeting)
            binding.clickListener = View.OnClickListener{
                fragment.findNavController().navigate(R.id.action_meetingsFragment_to_meetingFragment, bundleOf(
                    "meetingId" to meeting.id,
                    "meetingName" to meeting.name
                ))
            }
            binding.executePendingBindings()
        }
    }
}