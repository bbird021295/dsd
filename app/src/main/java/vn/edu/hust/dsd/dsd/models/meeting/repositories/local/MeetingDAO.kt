package vn.edu.hust.dsd.dsd.models.meeting.repositories.local

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.google.gson.Gson
import vn.edu.hust.dsd.dsd.models.meeting.entities.MeetingMemberModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.MeetingModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.MemberModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Meeting
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.MeetingMember

@Dao
abstract class MeetingDAO{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveMeeting(vararg meeting: Meeting)
    @Update
    abstract fun updateMeeting(vararg meeting: Meeting)


    @Query("select meetings.*, users.username as ownerUsername, users.fullName as ownerFullName from meetings left join users where meetings.ownerId = users.id")
    @Transaction()
    abstract fun browseMeetings(): LiveData<List<MeetingModel>>

    fun fetchMeetingsByMember(memberId: String): LiveData<List<MeetingModel>> =
        Transformations.switchMap(fetchMeetingsIdByMember(memberId)){meetingsId ->
            fetchMeetingsByIds(meetingsId)
        }

    @Query("select meetings.*, users.username as ownerUsername, users.fullName as ownerFullName from meetings left join users where meetings.ownerId = users.id and meetings.id in(:ids) order by meetings.created desc")
    @Transaction()
    protected abstract fun fetchMeetingsByIds(ids: List<String>): LiveData<List<MeetingModel>>

    @Query("select meetingId from user_join_meeting where memberId = :memberId")
    protected abstract fun fetchMeetingsIdByMember(memberId: String): LiveData<List<String>>

    @Query("select meetings.*, users.username as ownerUsername, users.fullName as ownerFullName from meetings left join users where meetings.ownerId = users.id and meetings.ownerId = :ownerId")
    @Transaction()
    abstract fun fetchMeetingsByOwner(ownerId: String): LiveData<List<MeetingModel>>

    @Query("select meetings.*, users.username as ownerUsername, users.fullName as ownerFullName from meetings left join users where meetings.ownerId = users.id and meetings.id = :meetingId")
    @Transaction()
    abstract fun fetchMeetingById(meetingId: String): LiveData<MeetingModel>

    @Delete()
    abstract fun deleteMeeting(meeting: Meeting)

    @Insert(onConflict = REPLACE)
    abstract fun saveMeetingMember(vararg meetingMembers: MeetingMember)

    @Query("select * from user_join_meeting")
    abstract fun browseMeetingsMembers(): LiveData<List<MeetingMember>>

    @Insert(onConflict = REPLACE)
    abstract fun saveMeetingMembers(meetingMembers: List<MeetingMember>): List<Long>

    @Insert(onConflict = REPLACE)
    abstract fun saveMeetings(list: List<Meeting>)


    @Query("select members.id as memberId, members.username as memberName, user_join_meeting.added as memberAdded, granters.id as granterId, granters.username as granterName, permissions.type as permissionType from user_join_meeting, users as members, users as granters, permissions where user_join_meeting.meetingId = :meetingId and user_join_meeting.memberId = members.id and permissions.meetingId = :meetingId and permissions.memberId = members.id and permissions.granterId = granters.id")
    abstract fun fetchMeetingMembers(meetingId: String): LiveData<List<MeetingMemberModel>>

    @Query("delete from user_join_meeting where meetingId = :meetingId")
    abstract fun removeMembers(meetingId: String)


    @Query("select users.*, permissions.type as permissionType from users left join user_join_meeting, permissions where users.id = user_join_meeting.memberId and user_join_meeting.meetingId = :meetingId and permissions.meetingId = :meetingId and permissions.memberId = users.id")
    abstract fun fetchMembers(meetingId: String): LiveData<List<MemberModel>>
}