package vn.edu.hust.dsd.dsd.viewmodels

import android.util.Log
import androidx.lifecycle.*
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import vn.edu.hust.dsd.dsd.models.meeting.entities.MeetingMemberModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.PermissionType
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.TypeConverterUtil
import vn.edu.hust.dsd.dsd.models.meeting.repositories.MeetingRepository
import vn.edu.hust.dsd.dsd.models.user.entities.User
import vn.edu.hust.dsd.dsd.models.user.repositories.UserRepository

class MeetingMembersViewModel(private val meetingId: String, private val meetingRepository: MeetingRepository, private val userRepository: UserRepository): ViewModel(){
    private val currentUser: LiveData<User> = userRepository.fetchCurrentUser()
    val members: LiveData<List<MeetingMemberModel>> = meetingRepository.fetchMeetingMembers(meetingId)
    val newMemberUsername: MutableLiveData<String> = MutableLiveData<String>().apply{
        value = "xxx"
    }
    val newMember: LiveData<User> = Transformations.switchMap(newMemberUsername){username ->
        username?.let{
            userRepository.getUserByUsername(username)
        }
    }
    val newMemberUsernameAvailable: LiveData<Boolean> = Transformations.map(newMember){
        it != null
    }
    val newMemberUsernameErrorMessage: LiveData<String> = Transformations.map(newMemberUsernameAvailable){available ->
        when{
            !available -> "Người dùng không tồn tại"
            else -> null
        }
    }
    val permissionTypes: List<String> = listOf(
        "ReadOnly", "ReadWrite", "Manage"
    )

    val adding: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply{
        value = false
    }
    val addable: LiveData<Boolean> = MediatorLiveData<Boolean>().apply{
        value = false
        addSource(newMemberUsernameAvailable){
            value = newMemberUsernameAvailable.value == true && adding.value == false
        }
        addSource(adding){
            value = newMemberUsernameAvailable.value == true && adding.value == false
        }
    }
    init{
        newMember.observeForever {
            Log.e("New Member ID: ", Gson().toJson(it))
        }
    }
    fun addMember(permissionType: String?, onSuccess: () -> Unit, onFail: (Throwable?) -> Unit){
        adding.value = true
        val newMemberId: String ?= newMember.value?.id
        val currentUserId: String ? = currentUser.value?.id
        when {
            newMemberId == null -> onFail(Exception("New member không khả dụng"))
            currentUserId == null -> onFail(Exception("Granter- Current user không khả dụng"))
            permissionType == null -> onFail(Exception("Permission type không khả dụng"))
            else -> {
                disposable = meetingRepository.addMeetingMember(meetingId,
                    MeetingRepository.AddMeetingMemberParams(
                        newMemberId,
                        currentUserId,
                        TypeConverterUtil().permissionTypeFromString(permissionType)
                    ).apply {
                        Log.e("Add Member Params", Gson().toJson(this))
                    }
                )
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        onSuccess()
                        adding.value = false
                    }, {
                        onFail(it)
                        adding.value = false
                    })
            }
        }

    }
    private var disposable: Disposable ?= null
    override fun onCleared() {
        super.onCleared()
        if(disposable?.isDisposed == false)
            disposable?.dispose()
    }

    class Factory(private val meetingId: String, private val meetingRepository: MeetingRepository, private val userRepository: UserRepository): ViewModelProvider.NewInstanceFactory(){
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return MeetingMembersViewModel(meetingId, meetingRepository, userRepository) as T
        }
    }
}