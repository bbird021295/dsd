package vn.edu.hust.dsd.dsd.viewmodels

import androidx.lifecycle.*
import vn.edu.hust.dsd.dsd.models.meeting.entities.PermissionType
import vn.edu.hust.dsd.dsd.models.meeting.entities.SpeechModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Permission
import vn.edu.hust.dsd.dsd.models.meeting.repositories.PermissionRepository
import vn.edu.hust.dsd.dsd.models.user.repositories.UserRepository
import vn.edu.hust.dsd.dsd.utils.TimeUtils

class MeetingSpeechViewModel(
    userRepository: UserRepository,
    permissionRepository: PermissionRepository
) : ViewModel(){
        val speech: MutableLiveData<SpeechModel> = MutableLiveData()
        fun setSpeech(speech: SpeechModel){
                this.speech.value = speech
        }
        val speechCreated : LiveData<String> = MediatorLiveData<String>().apply{
                addSource(speech){speech ->
                        value = speech.created?.let { TimeUtils.fromTimeInMillisToDate(it) }
                }
        }
        private val currentUser = userRepository.getCurrentUser()
        private val currentUserPermission: LiveData<Permission> = Transformations.switchMap(speech){speech ->
            if(currentUser != null && speech.meetingId != null)
                permissionRepository.fetchPermission(currentUser.id, speech.meetingId!!)
            else
                MutableLiveData<Permission>()
        }
        val currentUserEditable: LiveData<Boolean> = MediatorLiveData<Boolean>().apply{
                value = false
                addSource(speech){
                        value = speech.value?.speakerId == currentUser?.id || currentUserPermission.value?.type == PermissionType.Manage
                }
                addSource(currentUserPermission){
                    value = speech.value?.speakerId == currentUser?.id || currentUserPermission.value?.type == PermissionType.Manage
                }
        }
    val speakerFirstCharacterName: LiveData<String> = Transformations.map(speech){speech ->
        speech.speakerFullName?.split(" ")?.run {
            this.last().substring(0, 1)
        }
    }
        class Factory(private val userRepository: UserRepository, private val permissionRepository: PermissionRepository): ViewModelProvider.NewInstanceFactory(){
                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                    return MeetingSpeechViewModel(userRepository, permissionRepository) as T
                }
        }
}