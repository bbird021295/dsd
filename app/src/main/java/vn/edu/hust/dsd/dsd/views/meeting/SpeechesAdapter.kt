package vn.edu.hust.dsd.dsd.views.meeting

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.MeetingSpeechCardBinding
import vn.edu.hust.dsd.dsd.models.meeting.entities.SpeechModel
import vn.edu.hust.dsd.dsd.utils.InjectorUtils
import vn.edu.hust.dsd.dsd.viewmodels.MeetingSpeechViewModel
import java.lang.ref.WeakReference

class SpeechesAdapter(viewLifecycleOwner: LifecycleOwner) : ListAdapter<SpeechModel, SpeechesAdapter.ViewHolder>(object: DiffUtil.ItemCallback<SpeechModel>(){
        override fun areItemsTheSame(oldItem: SpeechModel, newItem: SpeechModel): Boolean {
                return oldItem.id == newItem.id
        }
        override fun areContentsTheSame(oldItem: SpeechModel, newItem: SpeechModel): Boolean {
                return oldItem == newItem
        }
}){
    private val viewLifecycleOwnerRef = WeakReference<LifecycleOwner>(viewLifecycleOwner)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: MeetingSpeechCardBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.meeting_speech_card, parent ,false)
        binding.setLifecycleOwner(viewLifecycleOwnerRef.get())
        return ViewHolder(binding, InjectorUtils.meetingSpeechViewModelFactory(parent.context).create(MeetingSpeechViewModel::class.java))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let{model ->
                holder.bind(model)
        }
    }

    class ViewHolder(private val binding: MeetingSpeechCardBinding, private val viewModel: MeetingSpeechViewModel): RecyclerView.ViewHolder(binding.root){
        fun bind(
            speech: SpeechModel
        ) {
            viewModel.setSpeech(speech)
            binding.viewModel = viewModel
            binding.meetingSpeechMoreClickListener = View.OnClickListener {
                EditSpeechDialog
                    .getInstance(itemView.context)
                    .show(speech, {})
            }
            binding.onClickListener = View.OnClickListener {
                Toast
                    .makeText(itemView.context, "Đến chi tiết SPEECH " + speech.id, Toast.LENGTH_SHORT)
                    .show()
            }
            binding.executePendingBindings()
        }
    }
}