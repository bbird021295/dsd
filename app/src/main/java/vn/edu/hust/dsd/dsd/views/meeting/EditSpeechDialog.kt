package vn.edu.hust.dsd.dsd.views.meeting

import android.content.Context
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.MeetingSpeechEditSpeechViewBinding
import vn.edu.hust.dsd.dsd.extensions.observeOnce
import vn.edu.hust.dsd.dsd.models.meeting.entities.SpeechModel
import vn.edu.hust.dsd.dsd.utils.InjectorUtils
import vn.edu.hust.dsd.dsd.viewmodels.EditSpeechViewModel

class EditSpeechDialog private constructor(context: Context){
    private val alertDialog: AlertDialog
    private val binding: MeetingSpeechEditSpeechViewBinding =
            DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.meeting_speech_edit_speech_view, null, false)
    private val toast: Toast = Toast.makeText(context.applicationContext, "", Toast.LENGTH_SHORT)

    init{
        alertDialog = AlertDialog.Builder(context)
                .setView(binding.root)
                .setCancelable(false)
                .create()
    }
    fun show(speechModel: SpeechModel, onClose: () -> Unit){
                val viewModel = InjectorUtils.editSpeechViewModelFactory(speechModel, binding.root.context).create(EditSpeechViewModel::class.java)
                viewModel.meetingMembersName.observeOnce {membersName ->
                        binding.meetingMembersNameValue = membersName
                }
                viewModel.speechContentErrorMessage.observeForever{message->
                        binding.speechContentErrorMessageValue = message
                }
                viewModel.editing.observeForever {editing ->
                        binding.editingValue = editing
                }
                viewModel.speechContentInvalid.observeForever {invalid ->
                        binding.speechContentInvalidValue = invalid
                }
                binding.speechContentMutableLiveData =  viewModel.speechContent
                binding.cancel.setOnClickListener {
                        close(onClose)
                }
                binding.accept.setOnClickListener {
                        viewModel.editing.value = true
                        viewModel.edit(binding.meetingSpeechSpeaker.selectedItem as String ?, {
                                toast.setText("Thành công")
                                toast.show()
                                viewModel.editing.value = false
                                close(onClose)
                        }, {error ->
                                toast.setText("Thất bại: " + error.message)
                                toast.show()
                                viewModel.editing.value = false
                        })
                }
                binding.executePendingBindings()
                alertDialog.show()
        }
        private fun close(onClose: () -> Unit){
                alertDialog.cancel()
                onClose()
        }
    companion object {
        @Volatile
        private var instance: EditSpeechDialog ?= null
        fun getInstance(context: Context) = instance ?: synchronized(this){
            instance ?: EditSpeechDialog(context).also {
                instance = it
            }
        }
    }
}
