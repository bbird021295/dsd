package vn.edu.hust.dsd.dsd.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.UserActivityBinding
import vn.edu.hust.dsd.dsd.utils.InjectorUtils

class UserActivity : AppCompatActivity() {
    private lateinit var binding:UserActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.user_activity)
    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.userFragmentNavHost).navigateUp()
    }

}
