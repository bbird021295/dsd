package vn.edu.hust.dsd.dsd.models.meeting.entities

import java.util.*

data class ImportMeetingSpeechModel(
    val id: String = UUID.randomUUID().toString(),
    val speaker: String, //username
    val content: String,
    val created: Long
)