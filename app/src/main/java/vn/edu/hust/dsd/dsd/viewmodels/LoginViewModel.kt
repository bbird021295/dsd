package vn.edu.hust.dsd.dsd.viewmodels

import android.util.Log
import androidx.lifecycle.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import vn.edu.hust.dsd.dsd.models.user.entities.User
import vn.edu.hust.dsd.dsd.models.user.repositories.UserRepository

class LoginViewModel(private val userRepository: UserRepository): ViewModel(){
    val currentUser: LiveData<User> = userRepository.fetchCurrentUser()

    val username: MutableLiveData<String> = MutableLiveData<String>().apply{
        value = "jlaotsezu"
    }
    val usernameError: LiveData<String?> = Transformations.map(username){
        validateUsernameError()
    }
    val password: MutableLiveData<String> = MutableLiveData<String>().apply{
        value = "123456"
    }
    val passwordError: LiveData<String?> = Transformations.map(username){
        validatePasswordError()
    }

    private var shouldShowError : Boolean = false

    val logging = MutableLiveData<Boolean>().apply{
        value = false
    }

    fun login(callback: LoginCallback){
        shouldShowError = true
        val invalid : Boolean = validateUsernameError() != null || validatePasswordError() != null
        if(!invalid) {
            logging.value = true
            userRepository.login(UserRepository.LoginParams(username.value!!, password.value!!))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ user ->
                    callback.onSuccess(user)
                    logging.value = false
                }, { error ->
                    callback.onFail(error)
                    Log.e(javaClass.simpleName, error.toString())
                    logging.value = false
                }).let{
                    disposables.add(it)
                }
        }
    }

    private var disposables = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        if(!disposables.isDisposed)
            disposables.dispose()
    }

    private fun validateUsernameError(): String?{
        return when{
            !shouldShowError -> null
            username.value.isNullOrBlank() -> "Username can't be empty."
            else -> null
        }
    }
    private fun validatePasswordError(): String?{
        return when{
            !shouldShowError -> null
            password.value.isNullOrBlank() -> "Password can't be empty."
            else -> null
        }
    }


    interface LoginCallback{
        fun onSuccess(user: User?)
        fun onFail(error: Throwable?)
    }

    class Factory(private val userRepository: UserRepository) : ViewModelProvider.NewInstanceFactory(){
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LoginViewModel(userRepository) as T
        }
    }
}