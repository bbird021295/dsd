package vn.edu.hust.dsd.dsd.models.core

import java.util.concurrent.Executors

private val executors = Executors.newCachedThreadPool()
internal fun collectThread(command: () -> Unit){
    executors.submit(command)
}