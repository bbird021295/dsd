package vn.edu.hust.dsd.dsd.models.meeting.entities

data class SpeechModel(
    var id: String ?= null,
    var created: Long ?= null,
    var content: String ?= null,
    var speakerId: String ?= null,
    var meetingId: String ?= null,
    var speakerUsername: String ?= null,
    var speakerFullName: String ?= null
)