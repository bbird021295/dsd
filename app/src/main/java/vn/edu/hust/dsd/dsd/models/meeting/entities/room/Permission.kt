package vn.edu.hust.dsd.dsd.models.meeting.entities.room

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import vn.edu.hust.dsd.dsd.models.meeting.entities.PermissionType
import vn.edu.hust.dsd.dsd.models.meeting.entities.firebase.FirebasePermission
import vn.edu.hust.dsd.dsd.models.user.entities.User

@Entity(
    tableName = "permissions",
    primaryKeys = ["memberId", "meetingId"],
//    foreignKeys = [
//        ForeignKey(entity = User::class, parentColumns = ["id"], childColumns = ["memberId"]),
//        ForeignKey(entity = Meeting::class, parentColumns = ["id"], childColumns = ["meetingId"]),
//        ForeignKey(entity = User::class, parentColumns = ["id"], childColumns = ["granterId"])
//    ],
    indices = [
        Index("memberId", "meetingId", "granterId")
    ]
)
data class Permission(
    val id: String,
    val memberId: String,
    val meetingId: String,
    val type: PermissionType,
    val granterId: String,
    val created: Long
)