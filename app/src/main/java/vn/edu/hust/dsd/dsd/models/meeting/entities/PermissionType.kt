package vn.edu.hust.dsd.dsd.models.meeting.entities

enum class PermissionType{
    ReadOnly,
    ReadWrite,
    Manage
}