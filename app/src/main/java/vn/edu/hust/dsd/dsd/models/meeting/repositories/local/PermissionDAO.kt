package vn.edu.hust.dsd.dsd.models.meeting.repositories.local

import androidx.lifecycle.LiveData
import androidx.room.*
import io.reactivex.Single
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Permission

@Dao
abstract class PermissionDAO{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun savePermission(vararg permission: Permission)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun savePermissions(permissions: List<Permission>)

    @Query("select * from permissions")
    abstract fun browsePermissions(): LiveData<List<Permission>>
    @Query("select * from permissions where meetingId = :meetingId and memberId = :memberId")
    abstract fun fetchPermissionByMemberAndMeeting(memberId: String, meetingId: String): LiveData<Permission>

    @Delete
    abstract fun deletePermission(permission: Permission)

    @Query("select * from permissions where meetingId = :meetingId and memberId = :id")
    abstract fun fetchPermissionByMemberAndMeetingSingle(id: String, meetingId: String): Single<Permission>


}