package vn.edu.hust.dsd.dsd.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import vn.edu.hust.dsd.dsd.models.meeting.repositories.MeetingRepository
import vn.edu.hust.dsd.dsd.models.user.repositories.UserRepository

class CreateMeetingViewModel(private val meetingRepository: MeetingRepository, private val userRepository: UserRepository): ViewModel(){
    val meetingName: MutableLiveData<String> = MutableLiveData<String>().apply{
        value = "Untitle 1"
    }
    val meetingNameError: LiveData<String> = Transformations.map(meetingName){
        validateMeetingNameError()
    }
    val meetingNameInvalid: LiveData<Boolean> = Transformations.map(meetingNameError){
        it != null
    }
    private fun validateMeetingNameError(): String?{
        return when{
            meetingName.value.isNullOrBlank() -> "Không được để trống"
            else -> null
        }
    }

    fun createMeeting(callback: CreateMeetingCallback){
        val invalid = validateMeetingNameError() != null
        if(!invalid) {
            meetingRepository.createMeeting(
                MeetingRepository.CreateMeetingParams(
                    meetingName.value!!,
                    userRepository.fetchCurrentUser().value!!.id
                )
            )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    callback.onSuccess(meetingName.value)
                }, {
                    callback.onFail(it)
                })
                .apply {
                    disposable = this
                }
        }
        else
            callback.onFail(Exception("Meeting name not available"))
    }

    override fun onCleared() {
        super.onCleared()
        if(disposable?.isDisposed == false)
            disposable?.dispose()
    }
    private var disposable: Disposable ?= null
    interface CreateMeetingCallback{
        fun onSuccess(meetingName: String?)
        fun onFail(error: Throwable?)
    }
}