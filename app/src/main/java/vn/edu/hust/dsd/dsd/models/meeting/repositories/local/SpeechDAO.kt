package vn.edu.hust.dsd.dsd.models.meeting.repositories.local

import androidx.lifecycle.LiveData
import androidx.room.*
import vn.edu.hust.dsd.dsd.models.meeting.entities.SpeechModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.firebase.FirebaseSpeech
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Speech

@Dao
abstract class SpeechDAO{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveSpeeches(speeches: List<Speech>)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveSpeech(vararg speech: Speech)

    @Query("select speeches.*, users.username as speakerUsername, users.fullName as speakerFullName from speeches left join users where speeches.speakerId = users.id")
    abstract fun browseSpeeches(): LiveData<List<SpeechModel>>
    @Query("select speeches.*, users.username as speakerUsername, users.fullName as speakerFullName from speeches left join users where speeches.speakerId = users.id and speeches.meetingId = :meetingId order by speeches.created desc")
    abstract fun fetchSpeechesByMeeting(meetingId: String): LiveData<List<SpeechModel>>

    @Delete
    abstract fun deleteSpeech(speech: Speech)

    @Query("select speeches.*, users.username as speakerUsername, users.fullName as speakerFullName from speeches left join users where speeches.speakerId = users.id and speeches.id = :speechId")
    abstract fun fetchSpeechById(speechId: String): LiveData<SpeechModel>
}