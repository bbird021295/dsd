package vn.edu.hust.dsd.dsd.utils

import android.content.Context
import vn.edu.hust.dsd.dsd.models.core.AppDatabase
import vn.edu.hust.dsd.dsd.models.meeting.entities.SpeechModel
import vn.edu.hust.dsd.dsd.models.meeting.repositories.*
import vn.edu.hust.dsd.dsd.models.meeting.repositories.remote.MeetingAPI
import vn.edu.hust.dsd.dsd.models.meeting.repositories.remote.SpeechAPI
import vn.edu.hust.dsd.dsd.models.user.repositories.UserRepositoryImpl
import vn.edu.hust.dsd.dsd.models.user.repositories.local.UserSharePrefs
import vn.edu.hust.dsd.dsd.models.user.repositories.remote.UserAPI
import vn.edu.hust.dsd.dsd.viewmodels.*
import vn.edu.hust.dsd.dsd.viewmodels.edittraces.MeetingEditTracesViewModel
import vn.edu.hust.dsd.dsd.viewmodels.edittraces.SpeechEditTracesViewModel
import vn.edu.hust.dsd.dsd.viewmodels.importmeeting.ImportMeetingParser
import vn.edu.hust.dsd.dsd.viewmodels.importmeeting.ImportMeetingViewModel
import vn.edu.hust.dsd.dsd.viewmodels.importmeeting.NaiveImportMeetingParser

object InjectorUtils{
        fun appDatabase(context: Context) = AppDatabase.getInstance(context)
        fun userDAO(context: Context) = appDatabase(context).userDAO()
        fun meetingDAO(context: Context) = appDatabase(context).meetingDAO()
        fun speechDAO(context: Context) = appDatabase(context).speechDAO()
        fun permissionDAO(context: Context) = appDatabase(context).permissionDAO()

        fun userSharePrefs(context: Context) = UserSharePrefs(context)
        fun userAPI() = UserAPI.getInstance()
        fun meetingAPI() = MeetingAPI.getInstance()
        fun speechAPI() = SpeechAPI.getInstance()
        fun userRepository(context: Context) = UserRepositoryImpl.getInstance(userAPI(), userDAO(context), userSharePrefs(context))
        fun meetingRepository(context: Context) = MeetingRepositoryImpl.getInstance(
            meetingAPI(),
            meetingDAO(context),
            userDAO(context)
        )
        fun speechRepository(context: Context) = SpeechRepositoryImpl.getInstance(speechAPI(), speechDAO(context), userDAO(context), permissionDAO(context))
        fun loginViewModelFactory(context: Context) = LoginViewModel.Factory(userRepository(context))
        fun registerViewModelFactory(context: Context) = RegisterViewModel.Factory(userRepository(context))

        fun meetingsViewModelFactory(context: Context) = MeetingsViewModel.Factory(meetingRepository(context), userRepository(context))
        fun meetingViewModelFactory(meetingId: String, context: Context) = MeetingViewModel.Factory(meetingId, meetingRepository(context), userRepository(context), speechRepository(context), permissionRepository(context))

        fun permissionRepository(context: Context): PermissionRepository = PermissionRepositoryImpl.getInstance(permissionDAO(context))

        fun meetingMembersViewModelFactory(meetingId: String, context: Context) = MeetingMembersViewModel.Factory(meetingId, meetingRepository(context), userRepository(context))
        fun meetingSpeechViewModelFactory(context: Context) =
        MeetingSpeechViewModel.Factory(userRepository(context), permissionRepository(context))

        fun editSpeechViewModelFactory(speechModel: SpeechModel, context: Context) = EditSpeechViewModel.Factory(speechModel, meetingRepository(context), speechRepository(context), userRepository(context), permissionRepository(context))

        fun editTracesViewModelFactory(context: Context) = MeetingEditTracesViewModel.Factory(editTraceRepository(context))
        fun editTraceDAO(context: Context) = appDatabase(context).editTraceDAO()
        private fun editTraceRepository(context: Context): EditTraceRepository {
                return EditTraceRepositoryImpl.getInstance(editTraceDAO(context))
        }

    fun speechEditTracesViewModelFactory(context: Context): SpeechEditTracesViewModel.Factory {
        return SpeechEditTracesViewModel.Factory(editTraceRepository(context))
    }

        fun importMeetingViewModel(context: Context): ImportMeetingViewModel.Factory {
                return ImportMeetingViewModel.Factory(
                    importMeetingParser(),
                    meetingRepository(context),
                    userRepository(context)
                )
        }

        private fun importMeetingParser(): ImportMeetingParser {
                return NaiveImportMeetingParser()
        }
}