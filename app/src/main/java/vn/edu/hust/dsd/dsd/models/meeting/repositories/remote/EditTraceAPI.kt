package vn.edu.hust.dsd.dsd.models.meeting.repositories.remote

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

interface EditTraceAPI{
    companion object {
        @Volatile
        private var instance : EditTraceAPI ?= null
        fun getInstance() = instance ?: synchronized(this){
            instance ?: Retrofit.Builder()
                .client(
                    OkHttpClient.Builder()
                        .connectTimeout(1, TimeUnit.MINUTES)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS)
                        .build()
                )
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://dsd-meeting-service.herokuapp.com/")
                .build()
                .create(EditTraceAPI::class.java).also {
                    instance = it
                }
        }
    }
}