package vn.edu.hust.dsd.dsd.viewmodels

import androidx.lifecycle.*
import vn.edu.hust.dsd.dsd.models.meeting.entities.MeetingModel
import vn.edu.hust.dsd.dsd.models.meeting.repositories.MeetingRepository
import vn.edu.hust.dsd.dsd.models.user.repositories.UserRepository

class MeetingsViewModel(private val meetingRepository: MeetingRepository, private val userRepository: UserRepository): ViewModel(){
    private val currentUser = userRepository.fetchCurrentUser()

    val meetings: LiveData<List<MeetingModel>> = Transformations.switchMap(currentUser){
        it?.let{user ->
            meetingRepository.getMeetingsByMember(user.id)
        }
    }
    val meetingsLoading: LiveData<Boolean> = Transformations.map(meetings){meetings ->
        meetings.isNullOrEmpty()
    }

    class Factory(private val meetingRepository: MeetingRepository, private val userRepository: UserRepository): ViewModelProvider.NewInstanceFactory(){

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return MeetingsViewModel(meetingRepository, userRepository) as T
        }
    }
}