package vn.edu.hust.dsd.dsd.views

import android.app.assist.AssistStructure.ViewNode.TEXT_STYLE_UNDERLINE
import android.os.Build
import android.view.View
import android.widget.ArrayAdapter
import android.widget.SpinnerAdapter
import android.widget.TextView
import androidx.appcompat.widget.AppCompatSpinner
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.textfield.TextInputLayout

@BindingAdapter("errorMessage")
fun errorMessage(textInputLayout: TextInputLayout, error: String?){
    textInputLayout.error = error
}
@BindingAdapter("textUnderline")
fun textUnderline(textView: TextView, underline: Boolean?){
    underline?.let {
        if(underline)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                textView.paintFlags = textView.paintFlags.or(TEXT_STYLE_UNDERLINE)
            }
        else
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.paintFlags = textView.paintFlags.minus(TEXT_STYLE_UNDERLINE)
        }
    }
}
@BindingAdapter("disabled")
fun disabledButton(button: MaterialButton, disabled: Boolean?){
    disabled?.let{
        button.isEnabled = !disabled
    }
}
@Suppress("UNCHECKED_CAST")
@BindingAdapter("listitem")
fun <T> listItem(recyclerView: RecyclerView, items: List<T>?){
    (recyclerView.adapter as ListAdapter<T, *>).submitList(items)
}
@BindingAdapter("visible")
fun visible(view: View, visible: Boolean?){
    if(visible == true)
        view.visibility = View.VISIBLE
    else if(visible == false)
        view.visibility = View.GONE
}
@BindingAdapter("unvisible")
fun unvisible(view: View, unvisible: Boolean?){
    if(unvisible == true)
        view.visibility = View.GONE
    else if(unvisible == false)
        view.visibility = View.VISIBLE
}
@BindingAdapter("hide")
fun hide(view: View, hide: Boolean?){
    if(hide == true)
        view.visibility = View.INVISIBLE
    else if(hide == false)
        view.visibility = View.VISIBLE
}
@BindingAdapter("chipMeetingMembers")
fun meetingMembersChips(chipGroup: ChipGroup, members: List<String>?){
    members?.let{safeMembers ->
        chipGroup.removeAllViews()
        for(member in safeMembers){
            val chip = Chip(chipGroup.context).apply{
                text = member
            }
            chipGroup.addView(chip)
        }
    }

}
@BindingAdapter("spinnerItems")
fun spinnerItems(spinner: AppCompatSpinner, members: List<String>?){
    members?.let{safeMembers ->
        spinner.adapter = ArrayAdapter<String>(spinner.context, android.R.layout.simple_spinner_item, safeMembers)
    }
}