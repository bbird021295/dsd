package vn.edu.hust.dsd.dsd.models.meeting.entities.room

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import vn.edu.hust.dsd.dsd.models.user.entities.User
import java.util.*

@Entity(
    tableName = "user_join_meeting",
    primaryKeys = ["memberId", "meetingId"],
//    foreignKeys = [
//        ForeignKey(entity = Meeting::class, parentColumns = ["id"], childColumns = ["meetingId"]),
//        ForeignKey(entity = User::class, parentColumns = ["id"], childColumns = ["memberId"])
//    ],
    indices = [Index("meetingId", "memberId")]
)
data class MeetingMember(
    val id: String = UUID.randomUUID().toString(),
    val memberId: String,
    val meetingId: String,
    val added: Long
)