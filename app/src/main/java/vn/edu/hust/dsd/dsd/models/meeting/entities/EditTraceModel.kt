package vn.edu.hust.dsd.dsd.models.meeting.entities

import androidx.room.Relation
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Meeting

data class EditTraceModel(
    var id: String?,
    var created: Long?,
    var oldValue: String?,
    var newValue: String?,
    var oldSpeaker: String?,
    var newSpeaker: String?,
    var speechId: String?,
    var editorId: String?,
    var editorName: String?,
    var meetingId: String?
)