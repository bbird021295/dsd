package vn.edu.hust.dsd.dsd.models.meeting.entities.room

import androidx.room.TypeConverter
import vn.edu.hust.dsd.dsd.models.meeting.entities.PermissionType
import vn.edu.hust.dsd.dsd.models.meeting.entities.firebase.FirebasePermission

class TypeConverterUtil{
    @TypeConverter
    fun permissionTypeFromString(value: String): PermissionType {
        return when(value){
            "Manage" -> PermissionType.Manage
            "ReadWrite" -> PermissionType.ReadWrite
            else -> PermissionType.ReadOnly
        }
    }
    @TypeConverter
    fun permissionTypeToString(type: PermissionType): String{
        return when(type){
            PermissionType.Manage -> "Manage"
            PermissionType.ReadWrite -> "ReadWrite"
            else -> "ReadOnly"
        }
    }

}