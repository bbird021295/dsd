package vn.edu.hust.dsd.dsd.viewmodels.importmeeting

import androidx.lifecycle.*
import vn.edu.hust.dsd.dsd.models.meeting.entities.ImportMeetingSpeechModel
import vn.edu.hust.dsd.dsd.models.user.repositories.UserRepository
import vn.edu.hust.dsd.dsd.utils.TimeUtils

class ImportMeetingSpeechViewModel(userRepository: UserRepository) : ViewModel(){
    fun setModel(model: ImportMeetingSpeechModel) {
        this.speech.value = model
    }
    private val speech: MutableLiveData<ImportMeetingSpeechModel> = MutableLiveData()
    val speaker: LiveData<String> = Transformations.map(speech){speech ->
        speech.speaker
    }
    val content: LiveData<String> = Transformations.map(speech){speech ->
        speech.content
    }
    val created: LiveData<String> = Transformations.map(speech){speech ->
        TimeUtils.fromTimeInMillisToDate(speech.created)
    }
    class Factory(val userRepository: UserRepository) : ViewModelProvider.NewInstanceFactory(){
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ImportMeetingSpeechViewModel(userRepository) as T
        }
    }
}