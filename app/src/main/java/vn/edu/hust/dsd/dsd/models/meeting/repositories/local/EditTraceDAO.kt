package vn.edu.hust.dsd.dsd.models.meeting.repositories.local

import androidx.lifecycle.LiveData
import androidx.room.*
import vn.edu.hust.dsd.dsd.models.meeting.entities.EditTraceModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.firebase.FirebaseEditTrace
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.EditTrace
@Dao
abstract class EditTraceDAO{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveEditTrace(vararg editTrace: EditTrace)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveEditTraces(editTraces: List<EditTrace>)

    @Query("select * from edit_traces")
    abstract fun browseRawEditTraces(): LiveData<List<EditTrace>>
    @Query("select edit_traces.*, editors.username, oldSpeakers.username as oldSpeaker, newSpeakers.username as newSpeaker  from edit_traces left join users as editors, users as oldSpeakers, users as newSpeakers where edit_traces.oldSpeakerId = oldSpeakers.id and edit_traces.newSpeakerId = newSpeakers.id and edit_traces.editorId = editors.id order by created desc")
    @Transaction
    abstract fun browseEditTraces(): LiveData<List<EditTraceModel>>
    @Query("""
        SELECT
            edit_traces.*,
            editors.username AS editorName,
            oldSpeakers.username AS oldSpeaker,
            newSpeakers.username AS newSpeaker
        FROM edit_traces
        LEFT JOIN
            users as editors,
            users as oldSpeakers,
            users as newSpeakers
        WHERE
            edit_traces.oldSpeakerId = oldSpeakers.id
            AND edit_traces.newSpeakerId = newSpeakers.id
            AND edit_traces.editorId = editors.id
            AND meetingId = :meetingId
        ORDER BY
            created DESC
        """)
    @Transaction
    abstract fun fetchEditTracesByMeeting(meetingId: String): LiveData<List<EditTraceModel>>
    @Query("select edit_traces.*, editors.username as editorName, oldSpeakers.username as oldSpeaker, newSpeakers.username as newSpeaker  from edit_traces left join users as editors, users as oldSpeakers, users as newSpeakers where edit_traces.oldSpeakerId = oldSpeakers.id and edit_traces.newSpeakerId = newSpeakers.id and speechId = :speechId  and edit_traces.editorId = editors.id order by created desc")
    @Transaction
    abstract fun fetchEditTracesBySpeech(speechId: String): LiveData<List<EditTraceModel>>

    @Delete
    abstract fun deleteEditTrace(editTrace: EditTrace)
}