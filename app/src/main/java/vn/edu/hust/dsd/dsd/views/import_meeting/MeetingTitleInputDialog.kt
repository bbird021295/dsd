package vn.edu.hust.dsd.dsd.views.import_meeting

import android.app.AlertDialog
import android.content.Context
import android.text.TextWatcher
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.ImportMeetingMeetingTitleInputBinding
import vn.edu.hust.dsd.dsd.views.meeting.TextWatcherAdapter

class MeetingTitleInputDialog(context: Context){
    private val dialog: AlertDialog
    private val binding: ImportMeetingMeetingTitleInputBinding = DataBindingUtil.inflate(
           LayoutInflater.from(context),
           R.layout.import_meeting_meeting_title_input,
           null,
           false
       )
    private val textWatcher: TextWatcher = object: TextWatcherAdapter(){
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            val error = if(s.isNullOrBlank()) "Tiêu đề không được để trống" else null
            binding.meetingTitleContainer.error = error
            binding.accept.isEnabled = error == null
        }
    }
    init{
        dialog = AlertDialog.Builder(context)
                .setView(binding.root)
                .setCancelable(false)
                .create()
        binding.meetingTitle.addTextChangedListener(textWatcher)
        binding.cancel.setOnClickListener {
            close()
        }
    }
    fun close(){
        binding.meetingTitle.removeTextChangedListener(textWatcher)
        dialog.cancel()
    }
    fun show(success: (String) -> Unit){
        binding.accept.setOnClickListener {
            val title = binding.meetingTitle.text.toString()
            success(title)
            close()
        }
        dialog.show()
    }
}