package vn.edu.hust.dsd.dsd.models.user.repositories

import androidx.lifecycle.LiveData
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import vn.edu.hust.dsd.dsd.models.user.entities.User
import vn.edu.hust.dsd.dsd.models.user.repositories.local.UserDAO
import vn.edu.hust.dsd.dsd.models.user.repositories.local.UserSharePrefs
import vn.edu.hust.dsd.dsd.models.user.repositories.remote.UserAPI

class UserRepositoryImpl(private val userAPI: UserAPI, private val userDAO: UserDAO, private val userSharePrefs: UserSharePrefs): UserRepository {
    override fun getCurrentUser(): User? {
        return userSharePrefs.getCurrentUser()
    }

    override fun getUserByUsername(username: String): LiveData<User> {
        return userDAO.fetchUserByUsername(username)
    }

    override fun getUserFullName(ownerId: String): LiveData<String> {
        return userDAO.fetchUserFullName(ownerId)
    }

    override fun getUser(id: String): LiveData<User> {
        return userDAO.fetchUserById(id)
    }

    override fun getUsersByIds(ids: List<String>): LiveData<List<User>> = userDAO.fetchUsersByIds(
        ids
    )

    override fun getUserInform(id: String): LiveData<User> = userDAO.fetchUserById(id)

    override fun logout(): Completable {
        return userSharePrefs
            .clearCurrentUser()
    }

    override fun fetchCurrentUser(): LiveData<User> = userSharePrefs.fetchCurrentUser()

    override fun register(params: UserRepository.RegisterParams): Single<User> = userAPI.register(params)
        .subscribeOn(Schedulers.io())

    override fun login(params: UserRepository.LoginParams): Single<User> = userAPI.login(params)
        .subscribeOn(Schedulers.io())
        .doOnSuccess{user ->
            userSharePrefs
                .postCurrentUser(user)
                .subscribe()
        }

    companion object {
        @Volatile
        private var instance: UserRepositoryImpl?= null
        fun getInstance(userAPI: UserAPI, userDAO: UserDAO, userSharePrefs: UserSharePrefs) = instance
            ?: synchronized(this){
            instance ?: UserRepositoryImpl(userAPI, userDAO, userSharePrefs).also {
                instance = it
            }
        }
    }
}