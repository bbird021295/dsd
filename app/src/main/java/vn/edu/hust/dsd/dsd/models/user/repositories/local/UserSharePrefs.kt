package vn.edu.hust.dsd.dsd.models.user.repositories.local

import android.content.Context
import android.content.Context.MODE_PRIVATE
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import io.reactivex.Completable
import vn.edu.hust.dsd.dsd.models.user.entities.User

class UserSharePrefs(context: Context){
    private val sharePrefs = context.getSharedPreferences("USER_SHARE_PREFS", MODE_PRIVATE)
    private val currentUser = MutableLiveData<User>().apply{
        val json: String? = sharePrefs.getString("currentUser", null)
        json?.let{
            value = Gson().fromJson(json, User::class.java)
        }
    }

    fun postCurrentUser(user: User): Completable {
        return Completable.fromAction {
            currentUser.postValue(user)
        }
    }
    fun setCurrentUser(user: User){
        currentUser.postValue(user)
    }
    fun getCurrentUser() = currentUser.value
    fun fetchCurrentUser(): LiveData<User> {
        return currentUser
    }
    fun clearCurrentUser(): Completable {
        return Completable.fromAction{
            currentUser.value = null
        }
    }
}