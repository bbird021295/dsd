package vn.edu.hust.dsd.dsd.viewmodels.importmeeting

import androidx.lifecycle.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import vn.edu.hust.dsd.dsd.models.meeting.entities.ImportMeetingSpeechModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Speech
import vn.edu.hust.dsd.dsd.models.meeting.repositories.MeetingRepository
import vn.edu.hust.dsd.dsd.models.user.repositories.UserRepository

class ImportMeetingViewModel(
    private val importMeetingParser: ImportMeetingParser,
    val meetingRepository: MeetingRepository,
    val userRepository: UserRepository
): ViewModel(){
    val whoFilePath: MutableLiveData<String> = MutableLiveData<String>().apply{
        value = "/who_file_path"
    }
    val whatFilePath: MutableLiveData<String> = MutableLiveData<String>().apply{
        value = "/what_file_path"
    }
    val speeches: LiveData<List<ImportMeetingSpeechModel>> = MediatorLiveData<List<ImportMeetingSpeechModel>>().apply{
        addSource(whoFilePath){
            value = importMeetingParser.parseFiles(whoFilePath.value, whatFilePath.value)
        }
        addSource(whatFilePath){
            value = importMeetingParser.parseFiles(whoFilePath.value, whatFilePath.value)
        }
    }
    val hasAnySpeech: LiveData<Boolean> = Transformations.map(speeches){speeches ->
        speeches.isNotEmpty()
    }
    val importing: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply{
        value = false
    }
    val importable: LiveData<Boolean> = MediatorLiveData<Boolean>().apply{
        value = false
        addSource(hasAnySpeech){
            value = hasAnySpeech.value == true && importing.value == false
        }
        addSource(importing){
            value = hasAnySpeech.value == true && importing.value == false
        }
    }

    fun importMeeting(title: String, onSuccess: () -> Unit, onFail: (Throwable) -> Unit) {
        val ownerId: String? = userRepository.getCurrentUser()?.id
        val speeches: List<ImportMeetingSpeechModel>? = this.speeches.value
        when{
            title.isBlank() -> onFail(Exception("Tiêu đề không được để trống"))
            ownerId.isNullOrBlank() -> onFail(Exception("Thông tin người dùng hiện tại không khả dụng"))
            speeches.isNullOrEmpty() -> onFail(Exception("Thông tin các phát biểu không khả dụng"))
            else -> meetingRepository.importMeeting(

                    title,
                    ownerId,
                    speeches

            )
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    importing.value = true
                }
                .doOnTerminate {
                    importing.value = false
                }
                .subscribe({
                    onSuccess()
                }, {error ->
                    onFail(error)
                }).run {
                    disposable = this
                }
        }
    }
    private var disposable: Disposable?= null
    override fun onCleared() {
        super.onCleared()
        if(disposable?.isDisposed == false)
            disposable?.dispose()
    }

    class Factory(
        val importMeetingParser: ImportMeetingParser,
        val meetingRepository: MeetingRepository,
        val userRepository: UserRepository
    ): ViewModelProvider.NewInstanceFactory(){
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ImportMeetingViewModel(importMeetingParser, meetingRepository, userRepository) as T
        }
    }
}