package vn.edu.hust.dsd.dsd.models.meeting.repositories

import androidx.lifecycle.LiveData
import vn.edu.hust.dsd.dsd.models.meeting.entities.EditTraceModel

interface EditTraceRepository{
    fun fetchEditTracesByMeeting(meetingId: String): LiveData<List<EditTraceModel>>
    fun fetchEditTracesBySpeech(speechId: String): LiveData<List<EditTraceModel>>
}