package vn.edu.hust.dsd.dsd.viewmodels

import android.util.Log
import androidx.lifecycle.*
import io.reactivex.android.schedulers.AndroidSchedulers
import vn.edu.hust.dsd.dsd.models.meeting.entities.MeetingMemberModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.PermissionType
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Permission
import vn.edu.hust.dsd.dsd.models.meeting.repositories.MeetingRepository
import vn.edu.hust.dsd.dsd.models.meeting.repositories.PermissionRepository
import vn.edu.hust.dsd.dsd.models.user.entities.User
import vn.edu.hust.dsd.dsd.models.user.repositories.UserRepository
import vn.edu.hust.dsd.dsd.utils.TimeUtils
import java.lang.RuntimeException

class MeetingMembersMemberViewModel(private val meetingId: String, private val meetingRepository: MeetingRepository, private val userRepository: UserRepository, private val permissionRepository: PermissionRepository) : ViewModel(){
        private val member = MutableLiveData<MeetingMemberModel>()
        fun setMember(member: MeetingMemberModel){
                this.member.value = member
        }
        val memberName: LiveData<String> = Transformations.map(member){
                it.memberName
        }
        val memberAdded: LiveData<String> = Transformations.map(member){
                "Ngày tham gia: " + TimeUtils.fromTimeInMillisToDate(it.memberAdded)
        }
        val granterName: LiveData<String> = Transformations.map(member){
                "Người giới thiệu: " + it.granterName
        }
        val permissionType: LiveData<String> = Transformations.map(member){
                "Quyền hạn: " + it.permissionType.toString()
        }
        private val currentUser: User? = userRepository.getCurrentUser()
        private val currentUserPermission: LiveData<Permission> = permissionRepository.fetchPermission(currentUser!!.id, meetingId)
        val currentUserIsManager: LiveData<Boolean> = Transformations.map(currentUserPermission){permission ->
                permission.type == PermissionType.Manage
        }
        val removing: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply{
                value = false
        }
        fun removeMember(onSuccess: () -> Unit, onFail: (Throwable) -> Unit){
                when{
                        member.value == null  || member.value!!.memberId.isNullOrBlank() -> throw RuntimeException("Member value not available.")
                        currentUser == null -> throw RuntimeException("Current user not available.")
                        else ->  {
                                removing.value = true
                                meetingRepository.removeMember(meetingId, member.value!!.memberId!!, currentUser.id)
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe({
                                                onSuccess()
                                                removing.value = false
                                        }, {
                                                onFail(it)
                                                removing.value = false
                                        })
                        }
                }
        }
}