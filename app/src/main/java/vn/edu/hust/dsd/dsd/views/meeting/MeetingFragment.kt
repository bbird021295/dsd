package vn.edu.hust.dsd.dsd.views.meeting


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.VERTICAL

import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.MeetingFragmentBinding
import vn.edu.hust.dsd.dsd.utils.InjectorUtils
import vn.edu.hust.dsd.dsd.viewmodels.MeetingViewModel


/**
 * A simple [Fragment] subclass.
 *
 */
class MeetingFragment : Fragment() {
    private lateinit var binding: MeetingFragmentBinding
    private lateinit var meetingId: String
    private lateinit var meetingName: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        meetingId = arguments!!.getString("meetingId")!!
        meetingName = arguments!!.getString("meetingName")!!
        (activity as AppCompatActivity).supportActionBar?.let{actionBar ->
            actionBar.title = meetingName
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.meeting_fragment, container, false)

        val factory = InjectorUtils.meetingViewModelFactory(meetingId, requireContext())
        val viewModel = ViewModelProviders.of(this, factory).get(MeetingViewModel::class.java)
        binding.viewModel = viewModel

        val adapter = SpeechesAdapter(viewLifecycleOwner)
        binding.meetingSpeeches.adapter = adapter
        binding.meetingSpeeches.layoutManager = LinearLayoutManager(requireContext(), VERTICAL, false).apply {
            reverseLayout = true
        }

        binding.createSpeech.setOnClickListener {
            val username: String? = binding.meetingSpeechSpeaker.selectedItem as String?
            val content: String? = binding.newSpeechContent.text.toString()
            when {
                username.isNullOrBlank() -> Toast.makeText(requireContext(), "Username không được để trống", Toast.LENGTH_SHORT).show()
                content.isNullOrBlank() -> Toast.makeText(requireContext(), "Nội dung không được để trống", Toast.LENGTH_SHORT).show()
                else -> {
                    hideKeyboard()
                    viewModel.createSpeech(username, content, {
                        Toast.makeText(requireContext(), "Thành công.", Toast.LENGTH_SHORT).show()
                        binding.newSpeechContent.text.clear()
                    }, {error ->
                        Toast.makeText(requireContext(), "Thất bại: " + error?.message, Toast.LENGTH_SHORT).show()
                    })
                }
            }
        }
        binding.meetingMembersManager.setOnClickListener {
            findNavController().navigate(R.id.action_meetingFragment_to_meetingMembersFragment, bundleOf(
                "meetingId" to meetingId,
                "meetingName" to meetingName
            ))
        }
        setupSeeLog()
        binding.setLifecycleOwner(viewLifecycleOwner)
        return binding.root
    }

    private fun setupSeeLog() {
        binding.seeLog.setOnClickListener {
            findNavController().navigate(R.id.action_meetingFragment_to_meetingEditTracesFragment, bundleOf(
                "meetingId" to meetingId
            ))
        }
    }

    private fun hideKeyboard() {
        val inputMethodManager: InputMethodManager? = getSystemService(requireActivity(), InputMethodManager::class.java)
        val focusedView: View = requireActivity().currentFocus ?: View(activity)
        val whatever = 0
        inputMethodManager?.hideSoftInputFromWindow(focusedView.windowToken, whatever)
    }


}
