package vn.edu.hust.dsd.dsd.views.edittraces

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.SpeechEditTraceCardBinding
import vn.edu.hust.dsd.dsd.models.meeting.entities.EditTraceModel
import vn.edu.hust.dsd.dsd.viewmodels.edittraces.SpeechEditTraceViewModel
import java.lang.ref.WeakReference

class SpeechEditTracesAdapter(private val navController: NavController, viewLifecycleOwner: LifecycleOwner) : ListAdapter<EditTraceModel, SpeechEditTracesAdapter.ViewHolder>(object: DiffUtil.ItemCallback<EditTraceModel>() {
    override fun areItemsTheSame(oldItem: EditTraceModel, newItem: EditTraceModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: EditTraceModel, newItem: EditTraceModel): Boolean {
        return oldItem == newItem
    }

}){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: SpeechEditTraceCardBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.speech_edit_trace_card,
            parent,
            false
        )
        return ViewHolder(binding, viewLifecycleOwnerRef.get())
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let{
            holder.bind(it)
        }
    }
    private val viewLifecycleOwnerRef = WeakReference(viewLifecycleOwner)
    class ViewHolder(private val binding: SpeechEditTraceCardBinding, viewLifecycleOwner: LifecycleOwner?): RecyclerView.ViewHolder(binding.root){
        private val viewModel: SpeechEditTraceViewModel = SpeechEditTraceViewModel()
        init{
            binding.setLifecycleOwner(viewLifecycleOwner)
            binding.viewModel = viewModel
        }
        fun bind(model: EditTraceModel){
            viewModel.setEditTrace(model)
        }
    }
}