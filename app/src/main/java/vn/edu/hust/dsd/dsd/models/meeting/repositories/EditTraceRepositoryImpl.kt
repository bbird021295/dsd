package vn.edu.hust.dsd.dsd.models.meeting.repositories

import androidx.lifecycle.LiveData
import vn.edu.hust.dsd.dsd.models.meeting.entities.EditTraceModel
import vn.edu.hust.dsd.dsd.models.meeting.repositories.local.EditTraceDAO

class EditTraceRepositoryImpl(private val editTraceDAO: EditTraceDAO) : EditTraceRepository {
    override fun fetchEditTracesBySpeech(speechId: String): LiveData<List<EditTraceModel>> {
        return editTraceDAO.fetchEditTracesBySpeech(speechId)
    }

    override fun fetchEditTracesByMeeting(meetingId: String): LiveData<List<EditTraceModel>> {
        return editTraceDAO.fetchEditTracesByMeeting(meetingId)
    }
    companion object {
        @Volatile
        private var instance: EditTraceRepository ?= null
        fun getInstance(editTraceDAO: EditTraceDAO) = instance ?: synchronized(this){
            instance ?: EditTraceRepositoryImpl(editTraceDAO).also {
                instance = it
            }
        }
    }
}