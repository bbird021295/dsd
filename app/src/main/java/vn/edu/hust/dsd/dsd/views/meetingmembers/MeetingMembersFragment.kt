package vn.edu.hust.dsd.dsd.views.meetingmembers


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders

import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.MeetingMembersFragmentBinding
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.TypeConverterUtil
import vn.edu.hust.dsd.dsd.utils.InjectorUtils
import vn.edu.hust.dsd.dsd.viewmodels.MeetingMembersViewModel

/**
 * A simple [Fragment] subclass.
 *
 */
class MeetingMembersFragment : Fragment() {
    private lateinit var binding: MeetingMembersFragmentBinding
    private lateinit var meetingId: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        meetingId = arguments!!.getString("meetingId")!!
        val meetingName = arguments!!.getString("meetingName")
        (activity as AppCompatActivity).supportActionBar?.title = meetingName
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.meeting_members_fragment, container, false)

        binding.meetingMembers.adapter = MeetingMembersAdapter(meetingId, viewLifecycleOwner)

        val factory = InjectorUtils.meetingMembersViewModelFactory(meetingId, requireContext())
        val viewModel = ViewModelProviders.of(this, factory).get(MeetingMembersViewModel::class.java)

        binding.viewModel = viewModel

        binding.addMeetingMember.setOnClickListener {
            viewModel.addMember(binding.permissions.selectedItem as String ?, {
                showMessage("Thành công")
            },
                {err->
                    showMessage("Error: " + err?.message)
                })
        }

        binding.setLifecycleOwner(viewLifecycleOwner)
        return binding.root
    }

    private fun showMessage(msesage: String) {
        Toast.makeText(context, msesage, Toast.LENGTH_SHORT)
            .show()
    }


}
