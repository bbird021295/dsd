package vn.edu.hust.dsd.dsd.viewmodels.edittraces

import androidx.lifecycle.*
import vn.edu.hust.dsd.dsd.models.meeting.entities.EditTraceModel
import vn.edu.hust.dsd.dsd.models.meeting.repositories.EditTraceRepository

class SpeechEditTracesViewModel(private val editTraceRepository: EditTraceRepository): ViewModel(){
    private val speechId: MutableLiveData<String> = MutableLiveData()
    val editTraces: LiveData<List<EditTraceModel>> = Transformations.switchMap(speechId){ speechId ->
        editTraceRepository.fetchEditTracesBySpeech(speechId)
    }
    fun setSpeechId(speechId: String){
        this.speechId.value = speechId
    }
    class Factory(private val editTraceRepository: EditTraceRepository): ViewModelProvider.NewInstanceFactory(){
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SpeechEditTracesViewModel(editTraceRepository) as T
        }
    }
}