package vn.edu.hust.dsd.dsd.views.meetingmembers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.MeetingMembersMemberCardBinding
import vn.edu.hust.dsd.dsd.models.meeting.entities.MeetingMemberModel
import vn.edu.hust.dsd.dsd.utils.InjectorUtils
import vn.edu.hust.dsd.dsd.viewmodels.MeetingMembersMemberViewModel
import java.lang.ref.WeakReference

class MeetingMembersAdapter(private val meetingId: String, viewLifecycleOwner: LifecycleOwner): ListAdapter<MeetingMemberModel, MeetingMembersAdapter.ViewHolder>(object: DiffUtil.ItemCallback<MeetingMemberModel>(){
    override fun areItemsTheSame(oldItem: MeetingMemberModel, newItem: MeetingMemberModel): Boolean {
        return oldItem.memberId == newItem.memberId
    }

    override fun areContentsTheSame(oldItem: MeetingMemberModel, newItem: MeetingMemberModel): Boolean {
        return oldItem == newItem
    }

}){
    private val viewLifecycleOwnerRef = WeakReference<LifecycleOwner>(viewLifecycleOwner)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: MeetingMembersMemberCardBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.meeting_members_member_card, parent, false)
        binding.setLifecycleOwner(viewLifecycleOwnerRef.get())
        return ViewHolder(
                binding
                , MeetingMembersMemberViewModel(meetingId, InjectorUtils.meetingRepository(parent.context), InjectorUtils.userRepository(parent.context), InjectorUtils.permissionRepository(parent.context))
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let{
            holder.bind(it)
        }
    }

    class ViewHolder(private val binding: MeetingMembersMemberCardBinding, private val viewModel: MeetingMembersMemberViewModel): RecyclerView.ViewHolder(binding.root){
        fun bind(model: MeetingMemberModel) {
            viewModel.setMember(model)
            binding.viewModel = viewModel
            binding.removeMember.setOnClickListener {
                viewModel.removeMember({
                    Toast.makeText(itemView.context, "Thành công", Toast.LENGTH_SHORT).show()
                }, {error ->
                    Toast.makeText(itemView.context, "Thất bại: " + error.message, Toast.LENGTH_SHORT).show()
                })
            }
            binding.executePendingBindings()
        }
    }
}