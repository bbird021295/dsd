package vn.edu.hust.dsd.dsd.views.edittraces


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.SpeechEditTracesFragmentBinding
import vn.edu.hust.dsd.dsd.utils.InjectorUtils
import vn.edu.hust.dsd.dsd.viewmodels.edittraces.SpeechEditTracesViewModel


/**
 * A simple [Fragment] subclass.
 *
 */
class SpeechEditTracesFragment : Fragment() {
    private lateinit var binding: SpeechEditTracesFragmentBinding
    private lateinit var viewModel: SpeechEditTracesViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.speech_edit_traces_fragment, container, false)
        setupViewModel()
        setupEditTraces()
        return binding.root
    }

    private fun setupViewModel() {
        val factory = InjectorUtils.speechEditTracesViewModelFactory(requireContext())
        viewModel = ViewModelProviders.of(this, factory).get(SpeechEditTracesViewModel::class.java)
        binding.viewModel = viewModel
        val speechId = arguments!!.getString("speechId")!!
        viewModel.setSpeechId(speechId)
        binding.setLifecycleOwner(viewLifecycleOwner)
    }
    private fun setupEditTraces() {
        binding.editTraces.adapter = SpeechEditTracesAdapter(findNavController(), viewLifecycleOwner)
        binding.editTraces.setHasFixedSize(true)
    }
}
