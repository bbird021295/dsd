package vn.edu.hust.dsd.dsd.models.meeting.entities.room

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
        tableName = "configurations",
//        foreignKeys = [
//                ForeignKey(entity = Meeting::class, parentColumns = ["id"], childColumns = ["meetingId"])
//        ],
        indices = [Index("meetingId")]
)
data class Configuration(
        @PrimaryKey
        val id: String,
        val meetingId: String
)