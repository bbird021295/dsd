package vn.edu.hust.dsd.dsd.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.google.gson.Gson
import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.HomeActivityBinding
import vn.edu.hust.dsd.dsd.models.user.entities.User
import vn.edu.hust.dsd.dsd.utils.InjectorUtils

class HomeActivity : AppCompatActivity() {
    private lateinit var binding: HomeActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.home_activity)
    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.mainFragmentNavHost).navigateUp()
    }
}
