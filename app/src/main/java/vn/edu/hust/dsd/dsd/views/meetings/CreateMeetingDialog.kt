package vn.edu.hust.dsd.dsd.views.meetings

import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.MeetingsCreateMeetingViewBinding
import vn.edu.hust.dsd.dsd.utils.InjectorUtils
import vn.edu.hust.dsd.dsd.viewmodels.CreateMeetingViewModel

class CreateMeetingDialog(context: Context){
    private val alertDialog: AlertDialog
    private val binding: MeetingsCreateMeetingViewBinding =
        DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.meetings_create_meeting_view, null, false)
    private val viewModel: CreateMeetingViewModel =
        CreateMeetingViewModel(InjectorUtils.meetingRepository(context), InjectorUtils.userRepository(context))
    private val toast: Toast = Toast.makeText(context, "", Toast.LENGTH_SHORT)
    init{
        binding.viewModel = viewModel
        alertDialog = AlertDialog.Builder(context)
            .setView(binding.root)
            .setCancelable(false)
            .create()
        binding.executePendingBindings()
    }
    fun show(onClose: () -> Unit){
        binding.cancel.setOnClickListener {
            close(onClose)
        }
        binding.accept.setOnClickListener {
            binding.accept.isEnabled = false
            binding.creatingProgressBar.visibility = View.VISIBLE
            viewModel.createMeeting(object: CreateMeetingViewModel.CreateMeetingCallback{
                override fun onSuccess(meetingName: String?) {
                    toast.setText("Tạo cuộc họp thành công: $meetingName")
                    binding.accept.isEnabled = true
                    binding.creatingProgressBar.visibility = View.GONE
                    close(onClose)
                }
                override fun onFail(error: Throwable?) {
                    binding.accept.isEnabled = true
                    binding.creatingProgressBar.visibility = View.GONE
                    error?.let{
                        toast.setText("Tạo cuộc họp thất bại: ${it.message}")
                        toast.show()
                    }
                }
            })
        }

        alertDialog.show()
    }
    private fun close(onClose: () -> Unit){
        alertDialog.cancel()
        onClose()
    }
}