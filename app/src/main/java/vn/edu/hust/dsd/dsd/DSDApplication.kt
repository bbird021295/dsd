package vn.edu.hust.dsd.dsd

import android.app.Application
import com.google.firebase.database.*
import vn.edu.hust.dsd.dsd.models.core.Collector
import vn.edu.hust.dsd.dsd.models.core.collectThread
import vn.edu.hust.dsd.dsd.models.meeting.entities.firebase.FirebaseMeeting
import vn.edu.hust.dsd.dsd.models.meeting.entities.firebase.FirebaseSpeech
import vn.edu.hust.dsd.dsd.models.meeting.entities.firebase.toRoomMeeting
import vn.edu.hust.dsd.dsd.models.meeting.entities.firebase.toRoomSpeech
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.MeetingMember
import vn.edu.hust.dsd.dsd.models.user.entities.FirebaseUser
import vn.edu.hust.dsd.dsd.models.user.entities.toRoomUser
import vn.edu.hust.dsd.dsd.utils.InjectorUtils
import java.util.*

class DSDApplication: Application(){
    override fun onCreate() {
        super.onCreate()

        Collector(applicationContext).run {
            collectUsers()
            collectMeetings()
            collectSpeeches()
            collectEditTraces()
            collectPermissions()
        }
    }

}