package vn.edu.hust.dsd.dsd.models.meeting.entities

data class MemberModel(
    var id: String?,
    var username: String?,
    var fullName: String?,
    var created: Long?,
    var permissionType: PermissionType
)