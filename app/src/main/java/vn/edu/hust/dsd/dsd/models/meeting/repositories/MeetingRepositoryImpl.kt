package vn.edu.hust.dsd.dsd.models.meeting.repositories

import androidx.lifecycle.LiveData
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import vn.edu.hust.dsd.dsd.models.meeting.entities.ImportMeetingSpeechModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.MeetingMemberModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.MeetingModel
import vn.edu.hust.dsd.dsd.models.meeting.repositories.local.MeetingDAO
import vn.edu.hust.dsd.dsd.models.meeting.repositories.remote.MeetingAPI
import vn.edu.hust.dsd.dsd.models.user.repositories.local.UserDAO
import java.lang.Exception

class MeetingRepositoryImpl(
    private val meetingAPI: MeetingAPI,
    private val meetingDAO: MeetingDAO,
    val userDAO: UserDAO
): MeetingRepository{
    override fun importMeeting(name: String, ownerId: String, speeches: List<ImportMeetingSpeechModel>): Completable {
        return Observable.fromIterable(speeches)
            .subscribeOn(Schedulers.io())
            .map {speech ->
                try {
                    val user = userDAO.getUserByUsername(speech.speaker)
                    MeetingRepository.ImportedSpeech(speech.id, user.id, speech.content, speech.created)
                }catch (exception: Exception){
                    throw Exception("Người dùng với username: ${speech.speaker} không tồn tại. Cần đăng ký tất cả tài khoản trước khi import")
                }
            }
            .toList()
            .flatMapCompletable {_speeches ->
                meetingAPI.importMeeting(
                    MeetingRepository.ImportMeetingParams(
                    name,
                    ownerId,
                    _speeches
                ))
            }
    }

    override fun removeMember(meetingId: String, memberId: String, granterId: String): Completable {
        return meetingAPI.removeMember(meetingId, memberId, granterId)
            .subscribeOn(Schedulers.io())
    }

    override fun fetchMeetingMembers(meetingId: String): LiveData<List<MeetingMemberModel>> {
        return meetingDAO.fetchMeetingMembers(meetingId)
    }

    override fun getMeetingById(meetingId: String): LiveData<MeetingModel> = meetingDAO.fetchMeetingById(meetingId)

    override fun createMeeting(params: MeetingRepository.CreateMeetingParams): Completable =
        meetingAPI
            .createMeeting(params)
            .subscribeOn(Schedulers.io())


    override fun updateMeeting(params: MeetingRepository.UpdateMeetingParams): Completable = meetingAPI.updateMeeting(params)
        .subscribeOn(Schedulers.io())

    override fun removeMeeting(meetingId: String): Completable = meetingAPI.removeMeeting(meetingId)
        .subscribeOn(Schedulers.io())

    override fun addMeetingMember(meetingId: String, params: MeetingRepository.AddMeetingMemberParams)
            : Completable = meetingAPI.addMeetingMember(meetingId, params)
        .subscribeOn(Schedulers.io())

    override fun addMeetingMembers(meetingId: String, params: MeetingRepository.AddMeetingMembersParams)
            : Completable = meetingAPI.addMeetingMembers(meetingId, params)
        .subscribeOn(Schedulers.io())

    override fun getMeetingsByOwner(ownerId: String): LiveData<List<MeetingModel>> = meetingDAO.fetchMeetingsByOwner(ownerId)

    override fun getMeetingsByMember(memberId: String): LiveData<List<MeetingModel>> = meetingDAO.fetchMeetingsByMember(memberId)

    companion object {
        @Volatile
        private var instance: MeetingRepository ?= null
        fun getInstance(
            meetingAPI: MeetingAPI,
            meetingDAO: MeetingDAO,
            userDAO: UserDAO
        ) = instance
            ?: synchronized(this){
            instance
                ?: MeetingRepositoryImpl(meetingAPI, meetingDAO, userDAO).also{
                instance = it
            }
        }
    }
}