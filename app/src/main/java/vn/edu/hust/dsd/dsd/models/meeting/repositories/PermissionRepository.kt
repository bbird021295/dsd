package vn.edu.hust.dsd.dsd.models.meeting.repositories

import androidx.lifecycle.LiveData
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Permission

interface PermissionRepository{
    fun fetchPermission(memberId: String, meetingId: String): LiveData<Permission>
}