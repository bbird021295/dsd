package vn.edu.hust.dsd.dsd.models.meeting.entities.firebase

import android.util.Log
import com.google.gson.Gson
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Meeting


class FirebaseMeeting{
    var id: String ?= null
    var name: String?= null
    var created: Long?= null
    var owner: FirebaseMeeting.User?= null
    var editTraces: Map<String, Long> ?= null
    var speeches: Map<String, Long> ?= null
    var members: Map<String, Long> ?= null
    var configuration: Configuration ?= null
    class User{
        var id: String ?= null
    }
    class Configuration{
        var id: String ?= null
    }
}
fun FirebaseMeeting.toRoomMeeting(): Meeting {
    //Log.e(javaClass.simpleName, "Meeting = " + Gson().toJson(this))
    return Meeting(
        this.id !!,
        this.name !!,
        this.created !!,
        this.owner!!.id!!
    )
}