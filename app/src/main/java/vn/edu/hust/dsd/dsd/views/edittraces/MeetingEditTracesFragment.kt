package vn.edu.hust.dsd.dsd.views.edittraces

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson

import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.MeetingEditTracesFragmentBinding
import vn.edu.hust.dsd.dsd.utils.InjectorUtils
import vn.edu.hust.dsd.dsd.viewmodels.edittraces.MeetingEditTracesViewModel

class MeetingEditTracesFragment : Fragment() {
    private lateinit var binding: MeetingEditTracesFragmentBinding
    private lateinit var viewModel: MeetingEditTracesViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.meeting_edit_traces_fragment, container, false)
        setupViewModel()
        setupEditTraces()
        return binding.root
    }

    private fun setupEditTraces() {
        binding.editTraces.adapter = MeetingEditTracesAdapter(findNavController(), viewLifecycleOwner)
        binding.editTraces.setHasFixedSize(true)
    }

    private fun setupViewModel() {
        val factory = InjectorUtils.editTracesViewModelFactory(requireContext())
        viewModel = ViewModelProviders.of(this, factory).get(MeetingEditTracesViewModel::class.java)
        binding.viewModel = viewModel
        val meetingId = arguments!!.getString("meetingId")!!
        viewModel.setMeetingId(meetingId)
        InjectorUtils.editTraceDAO(requireContext()).browseRawEditTraces().observeForever {editTraces ->
            for(editTrace in editTraces){
                Log.e("EditTrace: ", editTrace.toString())
            }
        }
        InjectorUtils.editTraceDAO(requireContext()).browseEditTraces().observeForever {editTraces ->
            for(editTrace in editTraces){
                Log.e("EditTrace: ", editTrace.toString())
            }
        }
        binding.setLifecycleOwner(viewLifecycleOwner)
    }
}
