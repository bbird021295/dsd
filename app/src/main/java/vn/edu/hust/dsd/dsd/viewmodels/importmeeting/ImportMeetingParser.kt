package vn.edu.hust.dsd.dsd.viewmodels.importmeeting

import vn.edu.hust.dsd.dsd.models.meeting.entities.ImportMeetingSpeechModel

interface ImportMeetingParser{
    fun parseFiles(whoFilePath: String?, whatFilePath: String?): List<ImportMeetingSpeechModel>
}
class NaiveImportMeetingParser: ImportMeetingParser{
    override fun parseFiles(whoFilePath: String?, whatFilePath: String?): List<ImportMeetingSpeechModel> {
        return listOf(
            ImportMeetingSpeechModel(
                speaker = "jlaotsezu",
                content = "Bắt đầu cuộc họp",
                created = System.currentTimeMillis()
            ),
            ImportMeetingSpeechModel(
                speaker = "jlaotsezu",
                content = "Chào",
                created = System.currentTimeMillis()
            ),
            ImportMeetingSpeechModel(
                speaker = "jlaotsezu",
                content = "Tất",
                created = System.currentTimeMillis()
            ),
            ImportMeetingSpeechModel(
                speaker = "jlaotsezu",
                content = "Cả",
                created = System.currentTimeMillis()
            ),
            ImportMeetingSpeechModel(
                speaker = "jlaotsezu",
                content = "Mọi",
                created = System.currentTimeMillis()
            ),
            ImportMeetingSpeechModel(
                speaker = "jlaotsezu",
                content = "Người",
                created = System.currentTimeMillis()
            )
        )
    }
}