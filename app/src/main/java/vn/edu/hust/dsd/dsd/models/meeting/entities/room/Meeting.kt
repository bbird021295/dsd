package vn.edu.hust.dsd.dsd.models.meeting.entities.room

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import vn.edu.hust.dsd.dsd.models.user.entities.User

@Entity(
    tableName = "meetings",
//    foreignKeys = [
//        ForeignKey(entity = User::class, parentColumns = ["id"], childColumns = ["ownerId"])
//    ],
    indices = [Index("ownerId")]
)
data class Meeting(
    @PrimaryKey
    val id: String,
    val name: String,
    val created: Long,
    val ownerId: String
)