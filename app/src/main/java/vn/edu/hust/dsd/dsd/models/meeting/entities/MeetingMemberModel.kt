package vn.edu.hust.dsd.dsd.models.meeting.entities

data class MeetingMemberModel(
    var memberId: String  ?= null,
    var memberName: String  ?= null,
    var memberAdded: Long  ?= null,
    var granterId: String  ?= null,
    var granterName: String  ?= null,
    var permissionType: PermissionType  ?= null
)