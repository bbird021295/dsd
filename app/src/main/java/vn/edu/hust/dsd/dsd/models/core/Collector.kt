package vn.edu.hust.dsd.dsd.models.core

import android.content.Context
import android.util.Log
import com.google.firebase.database.*
import com.google.gson.Gson
import vn.edu.hust.dsd.dsd.models.meeting.entities.firebase.*
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Meeting
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.MeetingMember
import vn.edu.hust.dsd.dsd.models.meeting.repositories.local.EditTraceDAO
import vn.edu.hust.dsd.dsd.models.meeting.repositories.local.MeetingDAO
import vn.edu.hust.dsd.dsd.models.meeting.repositories.local.PermissionDAO
import vn.edu.hust.dsd.dsd.models.meeting.repositories.local.SpeechDAO
import vn.edu.hust.dsd.dsd.models.user.entities.FirebaseUser
import vn.edu.hust.dsd.dsd.models.user.entities.toRoomUser
import vn.edu.hust.dsd.dsd.models.user.repositories.local.UserDAO
import vn.edu.hust.dsd.dsd.utils.InjectorUtils
import vn.edu.hust.dsd.dsd.utils.TimeUtils
import java.lang.RuntimeException
import java.util.*

class Collector(context: Context) {
    private val userDAO: UserDAO
    private val meetingDAO: MeetingDAO
    private val speechDAO: SpeechDAO
    private val editTraceDAO: EditTraceDAO
    private val permissionDAO: PermissionDAO
    init{
        val appDatabase = AppDatabase.getInstance(context)
        userDAO = appDatabase.userDAO()
        meetingDAO = appDatabase.meetingDAO()
        speechDAO = appDatabase.speechDAO()
        editTraceDAO = appDatabase.editTraceDAO()
        permissionDAO = appDatabase.permissionDAO()
    }

    fun collectPermissions(){
        val fireBaseDatabase = FirebaseDatabase.getInstance()
        val permissionRef = fireBaseDatabase.getReference("permissions")
        permissionRef.addChildEventListener(object: ChildEventListenerAdapter(){
            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                collectThread {
                    val permission = p0.getValue(FirebasePermission::class.java)
                    permission?.let{
                        permissionDAO.savePermission(permission.toRoomPermission())
                    }
                }
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                collectThread {
                    val permission = p0.getValue(FirebasePermission::class.java)
                    permission?.let{
                        permissionDAO.deletePermission(permission.toRoomPermission())
                    }
                }
            }
        })
    }

    fun collectEditTraces(){
        val fireBaseDatabase = FirebaseDatabase.getInstance()
        val editTraceRef = fireBaseDatabase.getReference("edittraces")
        editTraceRef.addChildEventListener(object: ChildEventListenerAdapter(){
            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                collectThread {
                    val editTrace = p0.getValue(FirebaseEditTrace::class.java)
                    editTrace?.let{
                        editTraceDAO.saveEditTrace(editTrace.toRoomEditTrace().apply{
                            Log.e("Edit Trace Room: ", Gson().toJson(this))
                        })
                    }
                }
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                collectThread {
                    val editTrace = p0.getValue(FirebaseEditTrace::class.java)
                    editTrace?.let{
                        editTraceDAO.deleteEditTrace(editTrace.toRoomEditTrace())
                    }
                }
            }
        })
    }

    fun collectSpeeches(){
        val firebaseDatabase = FirebaseDatabase.getInstance()
        val speechRef = firebaseDatabase.getReference("speeches")
        speechRef.addChildEventListener(object: ChildEventListenerAdapter() {
            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                collectThread {
                    val speech = p0.getValue(FirebaseSpeech::class.java)
                    speech?.let{
                        speechDAO.saveSpeech(speech.toRoomSpeech())
                    }
                }
            }
            override fun onChildRemoved(p0: DataSnapshot) {
                collectThread {
                    val speech = p0.getValue(FirebaseSpeech::class.java)
                    speech?.let{
                        speechDAO.deleteSpeech(speech.toRoomSpeech())
                    }
                }
            }
        })
    }

    fun collectUsers() {
        val fireBaseDatabase: FirebaseDatabase = FirebaseDatabase.getInstance()
        val userRef: DatabaseReference = fireBaseDatabase.getReference("users")
        userRef.addChildEventListener(object : ChildEventListenerAdapter() {
            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                collectThread {
                    val user = p0.getValue(FirebaseUser::class.java)
                    user?.let{
                        userDAO.saveUser(user.toRoomUser())
                    }
                }
            }
            override fun onChildRemoved(p0: DataSnapshot) {
                collectThread {
                    val user = p0.getValue(FirebaseUser::class.java)
                    user?.let {
                        userDAO.deleteUser(user.toRoomUser())
                    }
                }
            }
        })
    }

    companion object {
        private const val TAG = "DSDApplication"
    }

    fun collectMeetings() {
        val firebaseDatabase = FirebaseDatabase.getInstance()
        val meetingRef = firebaseDatabase.getReference("meetings")

        meetingRef.addChildEventListener(object : ChildEventListenerAdapter() {
            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                collectThread {
                    val firebaseMeeting = p0.getValue(FirebaseMeeting::class.java)
                    firebaseMeeting?.let {
                        val meeting = firebaseMeeting.toRoomMeeting()
                        val meetingMembers = firebaseMeeting.members?.keys?.map {memberId ->
                            MeetingMember(UUID.randomUUID().toString(), memberId, firebaseMeeting.id!!, firebaseMeeting.members?.get(memberId) ?: TimeUtils.curentTimeInMillis())
                        } ?: listOf()
                        meetingDAO.removeMembers(meeting.id)
                        meetingDAO.saveMeetingMembers(meetingMembers)
                        meetingDAO.saveMeeting(meeting)
                    }
                }
            }
            override fun onChildRemoved(p0: DataSnapshot) {
                collectThread {
                    val firebaseMeeting = p0.getValue(FirebaseMeeting::class.java)
                    firebaseMeeting?.let {
                        val meeting = firebaseMeeting.toRoomMeeting()
                        meetingDAO.deleteMeeting(meeting)
                        meetingDAO.removeMembers(meeting.id)
                    }
                }
            }
        })
    }
}
abstract class ChildEventListenerAdapter: ChildEventListener{
    override fun onCancelled(p0: DatabaseError) {
    }

    override fun onChildMoved(p0: DataSnapshot, p1: String?) {
    }

    override fun onChildChanged(p0: DataSnapshot, p1: String?) {
    }

    override fun onChildAdded(p0: DataSnapshot, p1: String?) {
        onChildChanged(p0, p1)
    }

    override fun onChildRemoved(p0: DataSnapshot) {
    }
}
abstract class ValueEventListenerAdapter: ValueEventListener{
    override fun onCancelled(p0: DatabaseError) {

    }

    override fun onDataChange(p0: DataSnapshot) {
    }
}