package vn.edu.hust.dsd.dsd.models.meeting.repositories

import androidx.lifecycle.LiveData
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Permission
import vn.edu.hust.dsd.dsd.models.meeting.repositories.local.PermissionDAO

class PermissionRepositoryImpl(private val permissionDAO: PermissionDAO) : PermissionRepository {
    override fun fetchPermission(memberId: String, meetingId: String): LiveData<Permission> {
        return permissionDAO.fetchPermissionByMemberAndMeeting(memberId, meetingId)
    }
    companion object {
        @Volatile
        private var instance: PermissionRepository ?= null
        fun getInstance(permissionDAO: PermissionDAO) = instance
            ?: synchronized(this){
                instance
                    ?: PermissionRepositoryImpl(permissionDAO).also{
                        instance = it
                    }
            }
    }
}