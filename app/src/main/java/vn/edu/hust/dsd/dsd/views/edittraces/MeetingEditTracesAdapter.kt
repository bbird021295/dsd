package vn.edu.hust.dsd.dsd.views.edittraces

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.MeetingEditTraceCardBinding
import vn.edu.hust.dsd.dsd.models.meeting.entities.EditTraceModel
import vn.edu.hust.dsd.dsd.viewmodels.edittraces.MeetingEditTraceViewModel
import java.lang.ref.WeakReference

class MeetingEditTracesAdapter(private val navController: NavController, viewLifecycleOwner: LifecycleOwner) : ListAdapter<EditTraceModel, MeetingEditTracesAdapter.ViewHolder>(object: DiffUtil.ItemCallback<EditTraceModel>() {
    override fun areItemsTheSame(oldItem: EditTraceModel, newItem: EditTraceModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: EditTraceModel, newItem: EditTraceModel): Boolean {
        return oldItem == newItem
    }

}){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: MeetingEditTraceCardBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.meeting_edit_trace_card,
            parent,
            false
        )
        return ViewHolder(binding, viewLifecycleOwnerRef.get())
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let{
            holder.bind(it, navController)
        }
    }
    private val viewLifecycleOwnerRef = WeakReference(viewLifecycleOwner)
    class ViewHolder(private val binding: MeetingEditTraceCardBinding, viewLifecycleOwner: LifecycleOwner?): RecyclerView.ViewHolder(binding.root){
        private val viewModel: MeetingEditTraceViewModel = MeetingEditTraceViewModel()
        init{
            binding.setLifecycleOwner(viewLifecycleOwner)
            binding.viewModel = viewModel
        }
        fun bind(
            model: EditTraceModel,
            navController: NavController
        ){
            viewModel.setEditTrace(model)
            binding.clickListener = View.OnClickListener {
                navController.navigate(R.id.action_meetingEditTracesFragment_to_speechEditTracesFragment, bundleOf(
                    "speechId" to model.speechId
                ))
            }
        }
    }
}