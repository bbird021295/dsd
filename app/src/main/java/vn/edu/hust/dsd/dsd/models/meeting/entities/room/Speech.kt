package vn.edu.hust.dsd.dsd.models.meeting.entities.room

import androidx.room.*
import vn.edu.hust.dsd.dsd.models.user.entities.User

@Entity(
        tableName = "speeches",
//        foreignKeys = [
//                ForeignKey(entity = User::class, parentColumns = ["id"], childColumns = ["speakerId"]),
//                ForeignKey(entity = Meeting::class, parentColumns = ["id"], childColumns = ["meetingId"])
//        ],
        indices = [
                Index("speakerId"), Index("meetingId")
        ]
)
data class Speech(
        @PrimaryKey
        val id: String,
        val created: Long,
        val content: String,
        val speakerId: String,
        val meetingId: String,
        val adderId: String
)