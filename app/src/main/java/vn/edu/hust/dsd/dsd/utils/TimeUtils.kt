package vn.edu.hust.dsd.dsd.utils

import java.util.*
import java.util.Calendar.DAY_OF_WEEK

object TimeUtils{
    fun fromTimeInMillisToDate(timeInMillis: Long?): String{
        val calendar = Calendar.getInstance().apply {
            timeInMillis?.let{
                setTimeInMillis(timeInMillis)
            }
        }
        return getDayofWeek(calendar.get(DAY_OF_WEEK)) + ", " + calendar.get(Calendar.DAY_OF_MONTH).toString() + "/" + calendar.get(Calendar.MONTH).toString() + "/" + calendar.get(Calendar.YEAR)
    }
    private fun getDayofWeek(dayOfWeek: Int): String{
        return when(dayOfWeek){
            2 -> "Thứ hai"
            3 -> "Thứ ba"
            4 -> "Thứ tư"
            5 -> "Thứ năm"
            6 -> "Thứ sáu"
            7 -> "Thứ bảy"
            else -> "Chủ nhật"
        }
    }

    fun curentTimeInMillis(): Long {
        return Calendar.getInstance().timeInMillis
    }
}