package vn.edu.hust.dsd.dsd.viewmodels

import android.util.Log
import androidx.lifecycle.*
import com.google.gson.Gson
import vn.edu.hust.dsd.dsd.models.meeting.entities.MeetingModel
import vn.edu.hust.dsd.dsd.models.user.entities.User
import vn.edu.hust.dsd.dsd.models.user.repositories.UserRepository
import vn.edu.hust.dsd.dsd.utils.TimeUtils

class MeetingsMeetingViewModel(meeting: MeetingModel): ViewModel(){
    val meetingName = meeting.name
    val ownerFullName = "Creator: " + (meeting.ownerFullName ?: "loading...")
    val meetingCreated = TimeUtils.fromTimeInMillisToDate(meeting.created!!)
    val meetingMembers = "Số thành viên: " + (meeting.members?.size ?: 0) + " thành viên"
    val meetingSpeeches = "Số phát biểu: " + (meeting.speeches?.size ?: 0) + " phát biểu"
}