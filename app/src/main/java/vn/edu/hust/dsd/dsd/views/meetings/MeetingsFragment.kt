package vn.edu.hust.dsd.dsd.views.meetings


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson

import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.MeetingsFragmentBinding
import vn.edu.hust.dsd.dsd.utils.InjectorUtils
import vn.edu.hust.dsd.dsd.viewmodels.CreateMeetingViewModel
import vn.edu.hust.dsd.dsd.viewmodels.MeetingsViewModel

class MeetingsFragment : Fragment() {
    private lateinit var binding: MeetingsFragmentBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as AppCompatActivity?)?.supportActionBar?.title = resources.getString(R.string.app_name)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.meetings_fragment, container, false)

        val factory = InjectorUtils.meetingsViewModelFactory(context!!)
        val viewModel = ViewModelProviders.of(this, factory).get(MeetingsViewModel::class.java)

        binding.viewModel = viewModel
        binding.createMeetingClick = View.OnClickListener {
            binding.createMeeting.isEnabled = false
            val onClose = {
                binding.createMeeting.isEnabled = true
            }
            CreateMeetingDialog(requireContext())
                .show(onClose)
        }
        binding.importMeetingClick = View.OnClickListener {
           findNavController().navigate(R.id.action_meetingsFragment_to_importMeetingFragment)
        }

        val meetingsAdapter = MeetingsAdapter(this)
        binding.meetingsAdapter = meetingsAdapter

        binding.setLifecycleOwner(viewLifecycleOwner)

        return binding.root
    }
}
