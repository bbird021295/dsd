package vn.edu.hust.dsd.dsd.views.register


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.utils.InjectorUtils
import vn.edu.hust.dsd.dsd.databinding.RegisterFragmentBinding
import vn.edu.hust.dsd.dsd.models.user.entities.User
import vn.edu.hust.dsd.dsd.viewmodels.RegisterViewModel


/**
 * A simple [Fragment] subclass.
 *
 */
class RegisterFragment : Fragment() {
    private lateinit var binding: RegisterFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.register_fragment, container, false)

        val factory = InjectorUtils.registerViewModelFactory(context!!)
        val viewModel = ViewModelProviders.of(this, factory).get(RegisterViewModel::class.java)

        binding.viewModel = viewModel
        binding.registerCallback = object: RegisterViewModel.RegisterCallback{
            override fun onFail(error: Throwable) {
                Toast.makeText(context, "Register fail because error: " + error.message, Toast.LENGTH_SHORT).show()
            }

            override fun onSuccess(user: User?) {
                Toast.makeText(context, "Register success, ${user?.fullName}, back -> Login Page", Toast.LENGTH_SHORT)
                    .show()
                findNavController().navigate(R.id.loginFragment)
            }
        }

        binding.setLifecycleOwner(viewLifecycleOwner)

        return binding.root
    }


}
