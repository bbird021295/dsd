package vn.edu.hust.dsd.dsd.models.meeting.repositories

import androidx.lifecycle.LiveData
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import vn.edu.hust.dsd.dsd.models.meeting.entities.PermissionType
import vn.edu.hust.dsd.dsd.models.meeting.entities.SpeechModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Speech
import vn.edu.hust.dsd.dsd.models.meeting.repositories.local.PermissionDAO
import vn.edu.hust.dsd.dsd.models.meeting.repositories.local.SpeechDAO
import vn.edu.hust.dsd.dsd.models.meeting.repositories.remote.SpeechAPI
import vn.edu.hust.dsd.dsd.models.user.entities.User
import vn.edu.hust.dsd.dsd.models.user.repositories.local.UserDAO

class SpeechRepositoryImpl(private val speechDAO: SpeechDAO, private val speechAPI: SpeechAPI, private val userDAO: UserDAO, private val permissionDAO: PermissionDAO): SpeechRepository{
    override fun updateSpeech(
        speechId: String,
        newContent: String,
        speakerName: String,
        editorId: String
    ): Completable {
        return Single.just(speakerName)
            .subscribeOn(Schedulers.io())
            .map{_speakerName ->
                userDAO.getUserByUsername(_speakerName)
            }
            .flatMapCompletable{user ->
                speechAPI.updateSpeech(
                    SpeechRepository.UpdateSpeechParams(
                        speechId,
                        newContent,
                        user.id,
                        editorId
                    )
                )
            }
    }

    override fun fetchSpeechById(speechId: String): LiveData<SpeechModel> {
        return speechDAO.fetchSpeechById(speechId)
    }

    override fun createSpeech(content: String, username: String, meetingId: String, adderId: String): Completable {
        return Single.fromCallable {
            userDAO.getUserByUsername(username)
        }
            .subscribeOn(Schedulers.io())
            .flatMapCompletable {user ->
                createSpeech(SpeechRepository.CreateSpeechParams(content, user.id, meetingId, adderId))
            }
    }

    override fun createSpeech(params: SpeechRepository.CreateSpeechParams): Completable {
        return speechAPI.createSpeech(params)
            .subscribeOn(Schedulers.io())
    }

    override fun fetchSpeechesByMeeting(meetingId: String): LiveData<List<SpeechModel>> {
        return speechDAO.fetchSpeechesByMeeting(meetingId)
    }

    companion object {
        @Volatile
        private var instance: SpeechRepository ?= null
        fun getInstance(speechAPI: SpeechAPI, speechDAO: SpeechDAO, userDAO: UserDAO, permissionDAO: PermissionDAO) = instance ?: synchronized(this){
            instance ?: SpeechRepositoryImpl(speechDAO, speechAPI, userDAO, permissionDAO).also {
                instance = it
            }
        }
    }
}