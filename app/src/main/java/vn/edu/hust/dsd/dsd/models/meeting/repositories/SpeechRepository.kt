package vn.edu.hust.dsd.dsd.models.meeting.repositories

import androidx.lifecycle.LiveData
import io.reactivex.Completable
import vn.edu.hust.dsd.dsd.models.meeting.entities.SpeechModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Speech

interface SpeechRepository{
    fun createSpeech(params: CreateSpeechParams): Completable
    fun createSpeech(content: String, username: String, meetingId: String, adderId: String): Completable
    fun updateSpeech(speechId: String,
                     newContent: String,
                     speakerName: String,
                     editorId: String)
            : Completable

    fun fetchSpeechesByMeeting(meetingId: String): LiveData<List<SpeechModel>>
    fun fetchSpeechById(speechId: String): LiveData<SpeechModel>

    class UpdateSpeechParams(
        val speechId: String,
        val newContent: String,
        val newSpeakerId: String,
        val editorId: String
    )
    class CreateSpeechParams(
        val content: String,
        val speakerId: String,
        val meetingId: String,
        val adderId: String
    )
}