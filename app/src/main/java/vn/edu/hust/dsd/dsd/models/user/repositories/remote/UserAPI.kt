package vn.edu.hust.dsd.dsd.models.user.repositories.remote

import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import vn.edu.hust.dsd.dsd.models.user.entities.User
import vn.edu.hust.dsd.dsd.models.user.repositories.UserRepository
import java.util.concurrent.TimeUnit

interface UserAPI{
    @POST("login")
    fun login(@Body loginParams: UserRepository.LoginParams): Single<User>
    @POST("register")
    fun register(@Body registerParams: UserRepository.RegisterParams): Single<User>

    companion object {
        @Volatile
        private var instance : UserAPI ?= null
        fun getInstance(): UserAPI = instance ?: synchronized(this){
            instance ?: Retrofit.Builder().apply {
                            addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                            addConverterFactory(GsonConverterFactory.create())
                            client(OkHttpClient.Builder()
                                .connectTimeout(1, TimeUnit.MINUTES)
                                .readTimeout(30, TimeUnit.SECONDS)
                                .writeTimeout(30, TimeUnit.SECONDS)
                                .build())
                            baseUrl("https://dsd-user-service.herokuapp.com/users/")
                        }
                .build()
                .create(UserAPI::class.java)
                .also {
                    instance = it
                }

        }
    }
}