package vn.edu.hust.dsd.dsd.viewmodels.edittraces

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.EditTraceModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.EditTrace
import vn.edu.hust.dsd.dsd.utils.TimeUtils

class MeetingEditTraceViewModel(): ViewModel(){
    private val editTrace: MutableLiveData<EditTraceModel> = MutableLiveData()
    val actorName: LiveData<String> = Transformations.map(editTrace){editTrace ->
        editTrace.editorName
    }
    val activity: LiveData<String> = Transformations.map(editTrace){editTrace ->
        val result = StringBuilder()
        if(editTrace.oldValue != editTrace.newValue) {
            result.append("Thay đổi nội dung phát biểu từ: " + editTrace.oldValue + " ->  " + editTrace.newValue)
            if(editTrace.newSpeaker != editTrace.oldSpeaker) {
                result.append("\n" + "Thay đổi người nói từ: " + editTrace.oldSpeaker + " -> " + editTrace.newSpeaker)
            }
            else{
                result.append("\nNgười nói: " + editTrace.newSpeaker)
            }
        }
        else{
            result.append("Thêm phát biểu: " + editTrace.newValue)
            if(editTrace.newSpeaker != editTrace.oldSpeaker){
                result.append("\n" + "Thay đổi người nói từ: " + editTrace.oldSpeaker + " -> " + editTrace.newSpeaker)
            }
            else{
                result.append("\nNgười nói: " + editTrace.newSpeaker)
            }
        }

        result.toString()
    }
    val activityCreated: LiveData<String> = Transformations.map(editTrace){editTrace ->
        TimeUtils.fromTimeInMillisToDate(editTrace.created)
    }
    fun setEditTrace(editTraceModel: EditTraceModel){
        editTrace.value = editTraceModel
    }
}