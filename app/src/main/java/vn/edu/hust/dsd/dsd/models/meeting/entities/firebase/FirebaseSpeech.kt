package vn.edu.hust.dsd.dsd.models.meeting.entities.firebase

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Speech


class FirebaseSpeech{
        var id: String ?= null
        var created: Long ?= null
        var content: String ?= null
        var speaker: User ?= null
        var adder: User ?= null
        var meeting: Meeting ?= null
        class User{
            var id: String ?= null
        }
        class Meeting{
                var id: String ?= null
        }
}
fun FirebaseSpeech.toRoomSpeech(): Speech{
        return Speech(this.id!!, this.created!!, this.content!!, this.speaker!!.id!!, this.meeting!!.id!!, this.adder!!.id!!
        )
}