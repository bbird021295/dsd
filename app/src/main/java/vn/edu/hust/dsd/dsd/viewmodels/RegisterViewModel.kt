package vn.edu.hust.dsd.dsd.viewmodels

import androidx.lifecycle.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import vn.edu.hust.dsd.dsd.models.user.entities.User
import vn.edu.hust.dsd.dsd.models.user.repositories.UserRepository

class RegisterViewModel(private val userRepository: UserRepository): ViewModel(){
    val username: MutableLiveData<String> = MutableLiveData<String>().apply{
        value = "jlaotsezu"
    }
    val usernameError: LiveData<String?> = Transformations.map(username){
        validateUsernameError()
    }
    val password: MutableLiveData<String> = MutableLiveData<String>().apply{
        value = "123456"
    }
    val passwordError: LiveData<String?> = Transformations.map(password){
        validatePasswordError()
    }
    val fullName: MutableLiveData<String> = MutableLiveData<String>().apply{
        value = "Tho Pham"
    }
    val fullNameError: LiveData<String?> = Transformations.map(fullName){
        validateFullNameError()
    }

    private var shouldShowError: Boolean = false

    private fun validateUsernameError(): String?{
        return when{
            !shouldShowError -> null
            username.value.isNullOrBlank() -> "Username can't be empty"
            else -> null
        }
    }
    private fun validatePasswordError(): String?{
        return when{
            !shouldShowError -> null
            password.value.isNullOrBlank() -> "Password can't be empty"
            password.value!!.length <= 4 -> "Password size should greater than or eq 5"
            else -> null
        }
    }
    private fun validateFullNameError(): String?{
        return when{
            !shouldShowError -> null
            fullName.value.isNullOrBlank() -> "FullName can't be empty"
            else -> null
        }
    }

    val registering = MutableLiveData<Boolean>().apply{
        value = false
    }

    fun register(callback: RegisterCallback){
        shouldShowError = true
        val invalid = validateUsernameError() != null
                    || validatePasswordError() != null
                    || validateFullNameError() != null
        if(!invalid) {
            registering.value = true
            userRepository.register(UserRepository.RegisterParams(username.value!!, password.value!!, fullName.value!!))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ user ->
                    callback.onSuccess(user)
                    registering.value = false
                }, { error ->
                    callback.onFail(error)
                    registering.value = false
                }).let{
                    disposables.add(it)
                }
        }
    }

    private var disposables = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        if(!disposables.isDisposed)
            disposables.dispose()
    }

    interface RegisterCallback{
        fun onSuccess(user: User?)
        fun onFail(error: Throwable)
    }

    class Factory(private val userRepository: UserRepository): ViewModelProvider.NewInstanceFactory(){
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return RegisterViewModel(userRepository) as T
        }
    }
}