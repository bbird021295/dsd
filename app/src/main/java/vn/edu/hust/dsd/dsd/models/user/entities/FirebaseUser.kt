package vn.edu.hust.dsd.dsd.models.user.entities

import android.util.Log
import com.google.gson.Gson


class FirebaseUser{
        var id: String ?= null
        var username: String ?= null
        var fullName: String ?= null
        var created: Long ?= null
}
fun FirebaseUser.toRoomUser(): User{
    return User(id = id!!, username = username!!, fullName = fullName!!, created = created!!).apply{
        Log.e("User Room: ", Gson().toJson(this))
    }
}