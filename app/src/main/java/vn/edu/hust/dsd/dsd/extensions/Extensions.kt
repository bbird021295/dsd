package vn.edu.hust.dsd.dsd.extensions

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer




fun <T> LiveData<T>.observeOnce(function: (T) -> Unit) {
        observeForever(object: Observer<T>{
                override fun onChanged(t: T) {
                        function(t)
                        removeObserver(this)
                }
        })
}
fun Fragment.getFilePath(uri: Uri): String?{
        if ("content".equals(uri.scheme, ignoreCase = true)) {
                val projection = arrayOf("_data")
                var cursor: Cursor? = null

                try {
                        cursor = requireContext().contentResolver.query(uri, projection, null, null, null)
                        val columnIndex = cursor.getColumnIndexOrThrow("_data")
                        if (cursor.moveToFirst()) {
                                return cursor.getString(columnIndex)
                        }
                } catch (e: Exception) {
                       Toast
                               .makeText(
                                       requireContext(),
                                       "Get File Path Error: " + e.message,
                                       Toast.LENGTH_SHORT)
                               .show()
                }
                finally {
                        cursor?.close()
                }

        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
                return uri.path
        }
        return null
}