package vn.edu.hust.dsd.dsd.models.meeting.entities.firebase

import vn.edu.hust.dsd.dsd.models.meeting.entities.PermissionType
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Permission


class FirebasePermission{
    var id: String ?= null
    var member: User ?= null
    var meeting: Meeting  ?= null
    var type: PermissionType  ?= null
    var granter: User ?= null
    var created: Long  ?= null

    class User{
            var id: String ?= null
    }
    class Meeting{
        var id: String ?= null
    }
}
fun FirebasePermission.toRoomPermission(): Permission{
    return Permission(
        id!!,
        member!!.id!!,
        meeting!!.id!!,
        type!!,
        granter!!.id!!,
        created!!
    )
}