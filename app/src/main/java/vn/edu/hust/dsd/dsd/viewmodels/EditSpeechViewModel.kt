package vn.edu.hust.dsd.dsd.viewmodels

import androidx.lifecycle.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import vn.edu.hust.dsd.dsd.models.meeting.entities.MeetingMemberModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.PermissionType
import vn.edu.hust.dsd.dsd.models.meeting.entities.SpeechModel
import vn.edu.hust.dsd.dsd.models.meeting.repositories.MeetingRepository
import vn.edu.hust.dsd.dsd.models.meeting.repositories.PermissionRepository
import vn.edu.hust.dsd.dsd.models.meeting.repositories.SpeechRepository
import vn.edu.hust.dsd.dsd.models.user.repositories.UserRepository

class EditSpeechViewModel(private val speechModel: SpeechModel, private val meetingRepository: MeetingRepository, private val speechRepository: SpeechRepository , private val userRepository: UserRepository, private val permissionRepository: PermissionRepository): ViewModel(){
        val speechContent: MutableLiveData<String> = MutableLiveData<String>().apply{
                value = speechModel.content
        }
        val speechContentErrorMessage: LiveData<String?> = Transformations.map(speechContent){content ->
                when{
                        content.isNullOrBlank() -> "Nội dung không được để trống"
                        else -> null
                }
        }
        val speechContentInvalid: LiveData<Boolean> = Transformations.map(speechContentErrorMessage){error ->
                error != null
        }
        fun setSpeechContent(value: String){
                speechContent.value = value
        }
        val editing: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply{
                value = false
        }
        val meetingMembers: LiveData<List<MeetingMemberModel>> =
                meetingRepository.fetchMeetingMembers(speechModel.meetingId!!)
        val meetingMembersName: LiveData<List<String>> = Transformations.map(meetingMembers){members ->
                members
                        .filter { member ->
                                member.permissionType != PermissionType.ReadOnly
                        }
                        .map {member ->
                        member.memberName!!
                }
        }
        private val currentUser = userRepository.getCurrentUser()
        fun edit(speakerName: String?, onSuccess: () -> Unit, onFail: (Throwable) -> Unit){
                editing.value = true
                val speechId: String? = speechModel.id
                val newContent: String? = speechContent.value
                val editorId : String? = currentUser?.id
                when{
                        speechId.isNullOrBlank() -> onFail(Exception("Speech ID not available"))
                        newContent.isNullOrBlank() -> onFail(Exception("New Content not available"))
                        speakerName.isNullOrBlank() -> onFail(Exception("Speaker ID not available"))
                        editorId.isNullOrBlank() -> onFail(Exception("Editor ID not available"))
                        else -> speechRepository.updateSpeech(
                                speechId,
                                newContent,
                                speakerName,
                                editorId
                        )
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                        onSuccess()
                                }, {
                                        onFail(it)
                                })
                                .run{disposable = this}
                }
        }
        override fun onCleared() {
                super.onCleared()
                if(disposable?.isDisposed == false)
                        disposable?.dispose()
        }

        private var disposable: Disposable ?= null

        class Factory( private val speechModel: SpeechModel, private val meetingRepository: MeetingRepository, private val speechRepository: SpeechRepository , private val userRepository: UserRepository, private val permissionRepository: PermissionRepository): ViewModelProvider.NewInstanceFactory(){
                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                        return EditSpeechViewModel(speechModel, meetingRepository, speechRepository, userRepository, permissionRepository) as T
                }
        }
}