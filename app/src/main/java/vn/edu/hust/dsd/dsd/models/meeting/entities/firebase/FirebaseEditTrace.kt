package vn.edu.hust.dsd.dsd.models.meeting.entities.firebase

import android.util.Log
import com.google.gson.Gson
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.EditTrace


class FirebaseEditTrace{
    var id: String?= null
    var created: Long ?= null
    var oldValue: String ?= null
    var newValue: String ?= null
    var oldSpeaker: User ?= null
    var newSpeaker: User ?= null
    var speech: Speech ?= null
    var editor: User ?= null
    var meeting: Meeting ?= null
    class Speech{
        var id: String ?= null
    }
    class User{
        var id: String ?= null
    }
    class Meeting{
        var id: String ?= null
    }
}
fun FirebaseEditTrace.toRoomEditTrace(): EditTrace{
    Log.e("Edit Trace: ", Gson().toJson(this))
    val room =  EditTrace(
        id!!,
        created!!,
        oldValue!!,
        newValue!!,
        oldSpeaker!!.id!!,
        newSpeaker!!.id!!,
        speech!!.id!!,
        editor!!.id!!,
        meeting!!.id!!
    )
    Log.e("Edit Trace Room: ", Gson().toJson(this))
    return room
}