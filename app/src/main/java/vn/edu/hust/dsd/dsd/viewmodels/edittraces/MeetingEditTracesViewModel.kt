package vn.edu.hust.dsd.dsd.viewmodels.edittraces

import androidx.lifecycle.*
import vn.edu.hust.dsd.dsd.models.meeting.entities.EditTraceModel
import vn.edu.hust.dsd.dsd.models.meeting.repositories.EditTraceRepository

class MeetingEditTracesViewModel(private val editTraceRepository: EditTraceRepository): ViewModel(){
    private val meetingId: MutableLiveData<String> = MutableLiveData()
    val editTraces: LiveData<List<EditTraceModel>> = Transformations.switchMap(meetingId){meetingId ->
        editTraceRepository.fetchEditTracesByMeeting(meetingId)
    }
    val anyResult: LiveData<Boolean> = Transformations.map(editTraces){editTraces ->
        editTraces.isNotEmpty()
    }
    fun setMeetingId(meetingId: String){
        this.meetingId.value = meetingId
    }
    class Factory(private val editTraceRepository: EditTraceRepository): ViewModelProvider.NewInstanceFactory(){
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return MeetingEditTracesViewModel(editTraceRepository) as T
        }
    }
}