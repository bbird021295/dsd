package vn.edu.hust.dsd.dsd.models.meeting.entities

import androidx.room.Embedded
import androidx.room.Relation
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.*
import vn.edu.hust.dsd.dsd.models.user.entities.User

data class MeetingModel(
    var id: String?,
    var name: String?,
    var created: Long?,
    var ownerId: String?,
    var ownerUsername: String?,
    var ownerFullName: String?,
    @Relation(
        entity = MeetingMember::class,
        parentColumn = "id",
        entityColumn = "meetingId",
        projection = ["memberId"]
    )
    var members: List<String>?,

    @Relation(
        entity = Speech::class,
        parentColumn = "id",
        entityColumn = "meetingId",
        projection = ["id"]
    )
    var speeches: List<String>?,
    @Relation(
        entity = EditTrace::class,
        parentColumn = "id",
        entityColumn = "meetingId",
        projection = ["id"]
    )
    var editTraces: List<String>?
)