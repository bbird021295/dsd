package vn.edu.hust.dsd.dsd.views.import_meeting

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.ImportMeetingSpeechCardBinding
import vn.edu.hust.dsd.dsd.models.meeting.entities.ImportMeetingSpeechModel
import vn.edu.hust.dsd.dsd.utils.InjectorUtils
import vn.edu.hust.dsd.dsd.viewmodels.importmeeting.ImportMeetingSpeechViewModel
import java.lang.ref.WeakReference

class SpeechesAdapter(viewLifeOwner: LifecycleOwner): ListAdapter<ImportMeetingSpeechModel, SpeechesAdapter.ViewHolder>(object: DiffUtil.ItemCallback<ImportMeetingSpeechModel>() {
    override fun areItemsTheSame(oldItem: ImportMeetingSpeechModel, newItem: ImportMeetingSpeechModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: ImportMeetingSpeechModel, newItem: ImportMeetingSpeechModel): Boolean {
        return oldItem == newItem
    }

}){
    private val viewLifecycleOwnerRef = WeakReference(viewLifeOwner)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let{
            holder.bind(it)
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ImportMeetingSpeechCardBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.import_meeting_speech_card,
            parent,
            false
        )
        return ViewHolder(binding, viewLifecycleOwnerRef.get())
    }

    class ViewHolder(
        binding: ImportMeetingSpeechCardBinding,
        viewLifeOwner: LifecycleOwner?
    ) : RecyclerView.ViewHolder(binding.root){
        private val viewModel: ImportMeetingSpeechViewModel
        init{
            viewModel = ImportMeetingSpeechViewModel(InjectorUtils.userRepository(binding.root.context))
            binding.viewModel = viewModel
            binding.setLifecycleOwner(viewLifeOwner)
        }
        fun bind(model: ImportMeetingSpeechModel){
            viewModel.setModel(model)
        }
    }
}