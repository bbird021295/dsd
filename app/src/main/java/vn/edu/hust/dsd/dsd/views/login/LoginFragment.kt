package vn.edu.hust.dsd.dsd.views.login


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.utils.InjectorUtils
import vn.edu.hust.dsd.dsd.databinding.LoginFragmentBinding
import vn.edu.hust.dsd.dsd.models.user.entities.User
import vn.edu.hust.dsd.dsd.viewmodels.LoginViewModel
import vn.edu.hust.dsd.dsd.views.HomeActivity

/**
 * A simple [Fragment] subclass.
 *
 */
class LoginFragment : Fragment() {
    private lateinit var binding: LoginFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.login_fragment, container, false)


        val factory = InjectorUtils.loginViewModelFactory(context!!)

        val viewModel = ViewModelProviders.of(this, factory).get(LoginViewModel::class.java)

        viewModel.currentUser.observe(viewLifecycleOwner, Observer {user ->
            user?.let{
                startActivity(Intent(requireContext(), HomeActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                })
            }
        })

        binding.viewModel = viewModel
        binding.loginCallback = object: LoginViewModel.LoginCallback{
            override fun onSuccess(user: User?) {
                Toast.makeText(context, "Login success, Hello ${user?.fullName}", Toast.LENGTH_SHORT).show()
            }

            override fun onFail(error: Throwable?) {
                Toast.makeText(context, "Login fail because error: ${error?.message}", Toast.LENGTH_SHORT).show()
            }

        }
        binding.registerClick = View.OnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
        binding.forgotPasswordClick = View.OnClickListener {
            Toast.makeText(context, "Not implemented!", Toast.LENGTH_SHORT).show()
        }

        binding.setLifecycleOwner(viewLifecycleOwner)

        return binding.root
    }
}
