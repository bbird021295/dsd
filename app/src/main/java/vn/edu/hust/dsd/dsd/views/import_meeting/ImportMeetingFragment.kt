package vn.edu.hust.dsd.dsd.views.import_meeting


import android.app.Activity.RESULT_OK
import android.content.ActivityNotFoundException
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders

import vn.edu.hust.dsd.dsd.R
import vn.edu.hust.dsd.dsd.databinding.ImportMeetingFragmentBinding
import vn.edu.hust.dsd.dsd.utils.InjectorUtils
import vn.edu.hust.dsd.dsd.viewmodels.importmeeting.ImportMeetingViewModel
import android.widget.Toast
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import vn.edu.hust.dsd.dsd.extensions.getFilePath


/**
 * A simple [Fragment] subclass.
 *
 */
class ImportMeetingFragment : Fragment() {
    private lateinit var binding: ImportMeetingFragmentBinding
    private lateinit var viewModel: ImportMeetingViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as AppCompatActivity?)?.supportActionBar?.title = "Import Meeting"
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.import_meeting_fragment, container, false)
        setupViewModel()
        setupSpeechesView()
        setupEvents()
        return binding.root
    }

    private fun setupEvents() {
        binding.who.setOnClickListener {
            showFileChooser("Chọn file chứa thông tin người nói", WHO_FILE_CHOOSER_CODE)
        }
        binding.what.setOnClickListener {
            showFileChooser("Chọn file chứa thông tin nội dung nói", WHAT_FILE_CHOOSER_CODE)
        }
        binding.importMeeting.setOnClickListener {
            showMeetingTitleInputDialog{title ->
                requestImportMeeting(title)
            }
        }
    }
    private fun showMeetingTitleInputDialog(onSuccess: (String) -> Unit){
        MeetingTitleInputDialog(requireContext())
            .show(onSuccess)
    }
    private fun requestImportMeeting(title: String){
        viewModel.importMeeting(title, {
            Toast.makeText(requireContext(), "Thành công", Toast.LENGTH_SHORT)
                .show()
            findNavController().popBackStack()
        }, { error ->
            Toast.makeText(requireContext(), "Thất bại: " + error.message, Toast.LENGTH_SHORT)
                .show()
        })
    }

    private fun showFileChooser(title: String?, code: Int){
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        intent.addCategory(Intent.CATEGORY_OPENABLE)

        try {
            startActivityForResult(
                Intent.createChooser(intent, title ?: "Chọn một file để import"),
                code
            )
        } catch (ex: ActivityNotFoundException) {
            // Potentially direct the user to the Market with a Dialog
            Toast
                .makeText(requireContext(), "Vui lòng cài đặt File Manager", Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if((requestCode == WHO_FILE_CHOOSER_CODE || requestCode == WHAT_FILE_CHOOSER_CODE)&& resultCode == RESULT_OK){
            val uri = data.data
            uri?.let{
                val path = getFilePath(uri)
                if(requestCode == WHO_FILE_CHOOSER_CODE)
                    binding.whoFileName.text = path
                else if(requestCode == WHAT_FILE_CHOOSER_CODE)
                    binding.whatFileName.text = path
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
    companion object {
        private const val WHO_FILE_CHOOSER_CODE = 21295
        private const val WHAT_FILE_CHOOSER_CODE = 21296
    }

    private fun setupSpeechesView() {
        binding.speechesAdapter = SpeechesAdapter(viewLifecycleOwner)
        binding.speeches.setHasFixedSize(true)
    }

    private fun setupViewModel() {
        val factory = InjectorUtils.importMeetingViewModel(requireContext())
       viewModel = ViewModelProviders.of(this, factory).get(ImportMeetingViewModel::class.java)
        binding.viewModel = viewModel
        binding.setLifecycleOwner(viewLifecycleOwner)
    }
}
