package vn.edu.hust.dsd.dsd.models.meeting.entities.room

import androidx.room.*
import vn.edu.hust.dsd.dsd.models.user.entities.User

@Entity(
    tableName = "edit_traces",
//    foreignKeys = [
//        ForeignKey(entity = Meeting::class, parentColumns = ["id"], childColumns = ["meetingId"]),
//        ForeignKey(entity = Speech::class, parentColumns = ["id"], childColumns = ["speechId"]),
//        ForeignKey(entity = User::class, parentColumns = ["id"], childColumns = ["editorId"])
//    ],
    indices = [Index("meetingId", "speechId", "editorId")]
)
data class EditTrace(
    @PrimaryKey
    val id: String,
    val created: Long,
    val oldValue: String,
    val newValue: String,
    val oldSpeakerId: String,
    val newSpeakerId: String,
    val speechId: String,
    val editorId: String,
    val meetingId: String
)