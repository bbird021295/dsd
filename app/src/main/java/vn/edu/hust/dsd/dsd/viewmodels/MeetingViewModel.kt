package vn.edu.hust.dsd.dsd.viewmodels

import androidx.lifecycle.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import vn.edu.hust.dsd.dsd.models.meeting.entities.MeetingModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.PermissionType
import vn.edu.hust.dsd.dsd.models.meeting.entities.SpeechModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Permission
import vn.edu.hust.dsd.dsd.models.meeting.repositories.MeetingRepository
import vn.edu.hust.dsd.dsd.models.meeting.repositories.PermissionRepository
import vn.edu.hust.dsd.dsd.models.meeting.repositories.SpeechRepository
import vn.edu.hust.dsd.dsd.models.user.entities.User
import vn.edu.hust.dsd.dsd.models.user.repositories.UserRepository
import vn.edu.hust.dsd.dsd.utils.TimeUtils

class MeetingViewModel(private val meetingId: String, private val meetingRepository: MeetingRepository, private val userRepository: UserRepository, private val speechRepository: SpeechRepository, private val permissionRepository: PermissionRepository): ViewModel(){
    private val currentUser: LiveData<User> = userRepository.fetchCurrentUser()
    private val currentUserPermission: LiveData<Permission> = Transformations.switchMap(currentUser){ user ->
        permissionRepository.fetchPermission(user.id, meetingId)
    }
    val currentUserIsManager: LiveData<Boolean> = Transformations.map(currentUserPermission){permission ->
        permission?.let{
            permission.type == PermissionType.Manage
        } ?: false
    }
    val currentUserPostable: LiveData<Boolean> = Transformations.map(currentUserPermission){permission ->
        permission?.let{
            permission.type != PermissionType.ReadOnly
        } ?: false
    }
    val meeting: LiveData<MeetingModel> = meetingRepository.getMeetingById(meetingId)
    val meetingCreated: LiveData<String> = Transformations.map(meeting){model ->
        TimeUtils.fromTimeInMillisToDate(model.created!!)
    }
    private val meetingMembersModel = Transformations.switchMap(meeting){ meeting ->
        meetingRepository.fetchMeetingMembers(meeting.id!!)
    }
    val meetingMembersAvailable: LiveData<List<String>> = Transformations.map(meetingMembersModel){meetings ->
        meetings
            .filter {member ->
                member.permissionType != PermissionType.ReadOnly
            }
            .map{member ->
            member.memberName!!
        }
    }
    val meetingMembers: LiveData<List<String>> = MediatorLiveData<List<String>>().apply{
        addSource(currentUserPermission){
            value = calculateMeetingMembers(currentUser.value, currentUserPermission.value, meetingMembersAvailable.value)
        }
        addSource(currentUser){
            value = calculateMeetingMembers(currentUser.value, currentUserPermission.value, meetingMembersAvailable.value)
        }
        addSource(meetingMembersAvailable){
            value = calculateMeetingMembers(currentUser.value, currentUserPermission.value, meetingMembersAvailable.value)
        }
    }

    private fun calculateMeetingMembers(
        user: User?,
        permission: Permission?,
        members: List<String>?
    ): List<String> {
        return if(user == null || permission == null) listOf()
        else if (permission.type == PermissionType.Manage)
            members ?: listOf(user.username)
        else
            listOf(user.username)

    }

    val meetingSpeeches: LiveData<List<SpeechModel>> = speechRepository.fetchSpeechesByMeeting(meetingId)

    val newSpeechContent = MutableLiveData<String>().apply {
        value = ""
    }
    val posting = MutableLiveData<Boolean>().apply{
        value = false
    }
    val postable = MediatorLiveData<Boolean>().apply{
        addSource(posting){
            value = posting.value == false && currentUserPostable.value == true
        }
        addSource(currentUserPostable){
            value = posting.value == false && currentUserPostable.value == true
        }
    }
    fun createSpeech(username: String, content: String, onSuccess: () -> Unit, onFail: (Throwable?) -> Unit){
        posting.value = true
        val currentUserId: String? = userRepository.getCurrentUser()?.id
        if(currentUserId.isNullOrBlank())
            onFail(Exception("Current user not available"))
        else
            speechRepository.createSpeech(content, username, meetingId, currentUserId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onSuccess()
                    posting.value = false
                }, {
                    onFail(it)
                    posting.value = false
                }).also{
                    disposable = it
                }
    }
    private var disposable: Disposable ?= null
    override fun onCleared() {
        super.onCleared()
        if(disposable?.isDisposed == false){
            disposable?.dispose()
        }
    }

    class Factory(private val meetingId: String, private val meetingRepository: MeetingRepository, private val userRepository: UserRepository, private val speechRepository: SpeechRepository, private val permissionRepository: PermissionRepository): ViewModelProvider.NewInstanceFactory(){
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return MeetingViewModel(meetingId, meetingRepository, userRepository, speechRepository, permissionRepository) as T
        }
    }
}