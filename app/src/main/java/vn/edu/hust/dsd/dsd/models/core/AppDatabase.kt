package vn.edu.hust.dsd.dsd.models.core

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.*
import vn.edu.hust.dsd.dsd.models.meeting.repositories.local.EditTraceDAO
import vn.edu.hust.dsd.dsd.models.meeting.repositories.local.MeetingDAO
import vn.edu.hust.dsd.dsd.models.meeting.repositories.local.PermissionDAO
import vn.edu.hust.dsd.dsd.models.meeting.repositories.local.SpeechDAO
import vn.edu.hust.dsd.dsd.models.user.entities.*
import vn.edu.hust.dsd.dsd.models.user.repositories.local.UserDAO

@Database(
    entities = [Configuration::class, EditTrace::class, Meeting::class, MeetingMember::class, Permission::class, Speech::class, User::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(TypeConverterUtil::class)
abstract class AppDatabase: RoomDatabase(){
    abstract fun userDAO(): UserDAO
    abstract fun meetingDAO(): MeetingDAO
    abstract fun speechDAO(): SpeechDAO
    abstract fun editTraceDAO(): EditTraceDAO
    abstract fun permissionDAO(): PermissionDAO
    companion object {
        private const val DATABASE_NAME = "dsd12"
        @Volatile
        private var instance: AppDatabase ?= null
        fun getInstance(context: Context) = instance ?: synchronized(this){
            instance ?: Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
                        .build()
                .also {
                    instance = it
                }
        }
    }
}