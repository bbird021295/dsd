package vn.edu.hust.dsd.dsd.models.user.repositories.local

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.*
import io.reactivex.Completable
import vn.edu.hust.dsd.dsd.models.core.AppDatabase
import vn.edu.hust.dsd.dsd.models.user.entities.User
import vn.edu.hust.dsd.dsd.models.user.repositories.UserRepository

@Dao
abstract class UserDAO{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveUser(vararg user: User)

    @Update
    abstract fun updateUser(vararg user: User)

    @Query("select * from users where id in(:ids)")
    abstract fun fetchUsersByIds(ids: List<String>): LiveData<List<User>>
    @Query("select * from users where id = :id")
    abstract fun fetchUserById(id: String): LiveData<User>

    @Delete
    abstract fun deleteUser(it: User)

    @Query("select * from users")
    abstract fun browseUsers(): LiveData<List<User>>

    @Query("select fullName from users where id = :ownerId")
    abstract fun fetchUserFullName(ownerId: String): LiveData<String>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveUsers(list: List<User>)

    @Query("select * from users where username = :username")
    abstract fun getUserByUsername(username: String): User

    @Query("select * from users where username = :username")
    abstract fun fetchUserByUsername(username: String): LiveData<User>

}