package vn.edu.hust.dsd.dsd.models.user.repositories

import androidx.lifecycle.LiveData
import io.reactivex.Completable
import io.reactivex.Single
import vn.edu.hust.dsd.dsd.models.user.entities.User

interface UserRepository{
    fun register(params: RegisterParams): Single<User>
    fun login(params: LoginParams): Single<User>
    fun logout(): Completable
    fun fetchCurrentUser(): LiveData<User>
    fun getCurrentUser(): User?
    fun getUserInform(id: String): LiveData<User>
    fun getUsersByIds(ids: List<String>): LiveData<List<User>>
    fun getUser(id: String): LiveData<User>
    fun getUserFullName(ownerId: String): LiveData<String>
    fun getUserByUsername(username: String): LiveData<User>

    class RegisterParams(
        var username: String,
        var password: String,
        var fullName: String
    )
    class LoginParams(
        var username: String,
        var password: String
    )
}