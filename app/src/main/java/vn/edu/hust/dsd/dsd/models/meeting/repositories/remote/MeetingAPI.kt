package vn.edu.hust.dsd.dsd.models.meeting.repositories.remote

import io.reactivex.Completable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import vn.edu.hust.dsd.dsd.models.meeting.repositories.MeetingRepository
import java.util.concurrent.TimeUnit


interface MeetingAPI{
    @POST("/meetings")
    fun createMeeting(@Body params: MeetingRepository.CreateMeetingParams): Completable

    @POST("/meetings/import")
    fun importMeeting(@Body params: MeetingRepository.ImportMeetingParams): Completable

    @DELETE("/meetings/{id}")
    fun removeMeeting(@Path("id") meetingId: String): Completable

    @POST("/meetings/{meetingId}/members")
    fun addMeetingMember(@Path("meetingId") meetingId: String, @Body params: MeetingRepository.AddMeetingMemberParams): Completable

    @POST("/meetings/{meetingId}/members")
    fun addMeetingMembers(@Path("meetingId") meetingId: String, @Body params: MeetingRepository.AddMeetingMembersParams): Completable

    @PUT("/meetings/{id}")
    fun updateMeeting(@Body params: MeetingRepository.UpdateMeetingParams): Completable

    @DELETE("/meetings/{id}/members/{memberId}")
    fun removeMember(@Path("id") meetingId: String, @Path("memberId") memberId: String, @Query("granterId") granterId: String): Completable

    companion object {
        @Volatile
        private var instance : MeetingAPI ?= null
        fun getInstance() = instance ?: synchronized(this){
            instance ?: Retrofit.Builder()
                .client(
                    OkHttpClient.Builder()
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .build()
                )
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://dsd-meeting-service.herokuapp.com/")
                .build()
                .create(MeetingAPI::class.java).also {
                    instance = it
                }
        }
    }
}