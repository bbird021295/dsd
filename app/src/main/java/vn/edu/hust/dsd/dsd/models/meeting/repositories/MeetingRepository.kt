package vn.edu.hust.dsd.dsd.models.meeting.repositories

import androidx.lifecycle.LiveData
import io.reactivex.Completable
import vn.edu.hust.dsd.dsd.models.meeting.entities.ImportMeetingSpeechModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.MeetingMemberModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.MeetingModel
import vn.edu.hust.dsd.dsd.models.meeting.entities.PermissionType
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.MeetingMember
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Permission
import vn.edu.hust.dsd.dsd.models.meeting.entities.room.Speech
import java.util.*

interface MeetingRepository{
    fun createMeeting(params: CreateMeetingParams): Completable
    fun importMeeting(name: String, ownerId: String, speeches: List<ImportMeetingSpeechModel>): Completable

    fun updateMeeting(params: UpdateMeetingParams): Completable

    fun removeMeeting(meetingId: String): Completable

    fun addMeetingMember(meetingId: String, params: AddMeetingMemberParams): Completable
    fun addMeetingMembers(meetingId: String, params: AddMeetingMembersParams): Completable

    fun getMeetingsByOwner(ownerId: String): LiveData<List<MeetingModel>>
    fun getMeetingsByMember(memberId: String): LiveData<List<MeetingModel>>
    fun getMeetingById(meetingId: String): LiveData<MeetingModel>
    fun fetchMeetingMembers(meetingId: String): LiveData<List<MeetingMemberModel>>
    fun removeMember(meetingId: String, memberId: String, granterId: String): Completable

    class AddMeetingMemberParams(
        var memberId: String,
        var granterId: String,
        var permissionType: PermissionType
    )
    class AddMeetingMembersParams(
        var memberParams: List<AddMeetingMemberParams>
    )
    class CreateMeetingParams(
        var name: String,
        var ownerId: String
    )
    class UpdateMeetingParams(
        var name: String
    )
    class ImportMeetingParams(
        var name: String,
        var ownerId: String,
        var speeches: List<ImportedSpeech>
    )
    class ImportedSpeech(
        val id: String = UUID.randomUUID().toString(),
        val speakerId: String,
        val content: String,
        val created: Long
    )
}