package vn.edu.hust.dsd.dsd.models.meeting.repositories.remote

import io.reactivex.Completable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import vn.edu.hust.dsd.dsd.models.meeting.repositories.SpeechRepository
import java.util.concurrent.TimeUnit

interface SpeechAPI{
    @POST("speeches")
    fun createSpeech(@Body params: SpeechRepository.CreateSpeechParams): Completable
    @PUT("speeches")
    fun updateSpeech(@Body params: SpeechRepository.UpdateSpeechParams): Completable

    companion object {
        private const val BASE_URL = "https://dsd-meeting-service.herokuapp.com/"
        @Volatile
        private var instance: SpeechAPI ?= null
        fun getInstance() = instance ?: synchronized(this){
            instance ?: Retrofit.Builder()
                .client(
                    OkHttpClient.Builder()
                        .connectTimeout(1, TimeUnit.MINUTES)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS)
                        .build()
                )
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(SpeechAPI::class.java)
                .also {
                    instance = it
                }
        }
    }
}