package vn.edu.hust.dsd.dsd.models.user.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(
    tableName = "users"
)
data class User(
    @PrimaryKey
    val id: String,
    val username: String,
    val fullName: String,
    val created: Long
)